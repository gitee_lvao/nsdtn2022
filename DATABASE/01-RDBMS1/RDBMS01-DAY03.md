### RDBMS1-DAY03-笔记

***

#### 子查询

##### 什么是子查询

SELECT语句中嵌套若干个SELECT子句从而完成某个复杂功能的SQL编写方法

##### 子查询出现的位置

- SELECT之后
- FROM之后
- WHERE之后
- HAVING之后

##### 子查询练习

```mysql
##server51操作

#登录MySQL服务
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

mysql> USE tarena;			#切换至tarena库

#使用子查询统计每个部门的人数(SELECT之后)
mysql> SELECT d.dept_id,d.dept_name FROM departments AS d;

mysql> SELECT COUNT(e.name) FROM employees AS e WHERE e.dept_id=1;

mysql> SELECT d.dept_id,d.dept_name,(SELECT COUNT(e.name) FROM employees AS e WHERE e.dept_id=d.dept_id) AS 人数 FROM departments AS d;

#查询3号部门的名称及其部门内员工的编号、名字和email
mysql> SELECT d.dept_name,e.* FROM departments AS d INNER JOIN employees AS e ON d.dept_id=e.dept_id;

mysql> SELECT dept_id,dept_name,employee_id,name,email FROM (SELECT d.dept_name,e.* FROM departments AS d INNER JOIN employees AS e ON d.dept_id=e.dept_id) AS tmp WHERE dept_id=3;

#查询运维部所有员工信息
mysql> SELECT dept_id,dept_name FROM departments WHERE dept_name="运维部";

mysql> SELECT * FROM employees WHERE dept_id=3;

mysql> SELECT * FROM employees WHERE dept_id=(SELECT dept_id FROM departments WHERE dept_name="运维部");

#部门人数比开发部少的部门id
mysql> SELECT dept_id FROM departments WHERE dept_name="开发部";

mysql> SELECT COUNT(name) FROM employees WHERE dept_id=4;

mysql> SELECT dept_id,COUNT(name) AS cnt FROM employees WHERE dept_id IS NOT NULL GROUP BY dept_id HAVING cnt<55;

mysql> SELECT dept_id,COUNT(name) AS cnt FROM employees WHERE dept_id IS NOT NULL GROUP BY dept_id HAVING cnt<(SELECT COUNT(name) FROM employees WHERE dept_id=(SELECT dept_id FROM departments WHERE dept_name="开发部"));
```

#### 库表管理

##### 库管理

```mysql
##server51操作：库管理相关操作

#登录MySQL服务
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

mysql> SHOW DATABASES;							#查看已有的库
	
mysql> SELECT DATABASE();						#查看当前正在操作的库，NULL表示未操作任何库

mysql> USE tarena;								#使用/切换到tarena库

mysql> SELECT DATABASE();						#查看当前正在操作的库

mysql> CREATE DATABASE testdb;					#创建testdb库

mysql> CREATE DATABASE TESTDB;					#创建TESTDB库，验证库名区分大小写

mysql> CREATE DATABASE testdb;					#创建重名库报错
ERROR 1007 (HY000): Can not create database 'testdb'; database exists
mysql> CREATE DATABASE IF NOT EXISTS testdb;	#加入判断IF NOT EXISTS为不存在则创建库	

mysql> SHOW DATABASES;							#查看已有的库

mysql> DROP DATABASE TESTDB;					#删除TESTDB库
Query OK, 0 rows affected (0.00 sec)

mysql> DROP DATABASE TESTDB;					#删除不存在的库报错
ERROR 1008 (HY000): Can't drop database 'TESTDB'; database doesn't exist
mysql> DROP DATABASE IF EXISTS TESTDB;			#加入判断IF EXISTS为存在则删除库

mysql> SHOW DATABASES;							#查看已有的库
```

##### 表管理

```mysql
#表管理相关操作

mysql> SELECT DATABASE();					#查看当前操作的库

mysql> SHOW TABLES;							#查看当前库下的表，前提是当前操作的库不是NULL

mysql> SHOW TABLES FROM mysql;				#查询指定库下的所有表

mysql> CREATE DATABASE studb;				#创建学员信息库studb

mysql> USE studb;							#使用studb库

mysql> SHOW TABLES;							#查看库下已有表，此时应为空库

mysql> CREATE TABLE studb.stuinfo(
    -> name CHAR(20),
    -> age INT,
    -> gender CHAR(4),
    -> class CHAR(9)
    -> );									#创建学员信息stuinfo表
    
mysql> SHOW TABLES;							#查看库下已有表

mysql> CREATE TABLE studb.stuinfo( name CHAR(20), age INT, gender CHAR(4), class CHAR(9) );									#创建同名表报错
ERROR 1050 (42S01): Table 'stuinfo' already exists
mysql> CREATE TABLE IF NOT EXISTS studb.stuinfo( name CHAR(20), age INT, gender CHAR(4), class CHAR(9) );					#加入IF NOT EXISTS判断，不存在则创建

mysql> DESC studb.stuinfo;					#查看表结构(看表头)

mysql> DROP TABLE studb.stuinfo;			#删除学员信息stuinfo表

mysql> DROP TABLE studb.stuinfo;			#删除不存在的表报错
ERROR 1051 (42S02): Unknown table 'studb.stuinfo'
mysql> DROP TABLE IF EXISTS studb.stuinfo;	#加入判断IF EXISTS，存在则删除
```

##### 修改表结构

```mysql
##语法格式
	ALTER TABLE 库名.表名 动作
		- ADD:		添加字段，默认最右边，可以通过操作符指定添加位置[AFTER|FIRST]
		- MODIFY:	修改字段类型、约束条件
		- CHANGE:	修改字段名称
		- DROP:		删除字段
		- RENAME:	重命名表

##修改表结构练习

#准备测试表
mysql> USE studb;				#使用studb库

mysql> SHOW TABLES;				#查看studb库下已有表

mysql> CREATE TABLE studb.stuinfo(name CHAR(20),age INT,gender CHAR(4));	#创建测试表

mysql> SHOW TABLES;				#查看studb库下已有表
    
mysql> DESC studb.stuinfo;		#查看stuinfo表结构

#添加字段
mysql> ALTER TABLE stuinfo ADD class CHAR(9);					#添加class字段

mysql> DESC stuinfo;		#class字段出现在最后

mysql> ALTER TABLE stuinfo ADD phone CHAR(11) AFTER gender;		#添加phone字段

mysql> DESC stuinfo;		#phone字段在gender和class中间

mysql> ALTER TABLE stuinfo ADD id INT FIRST;					#添加id字段

mysql> DESC stuinfo;		#id字段出现在最前边

#修改字段
mysql> ALTER TABLE stuinfo MODIFY name VARCHAR(50);				#修改name字段类型

mysql> DESC stuinfo;		#name字段类型从CHAR变为VARCHAR

mysql> ALTER TABLE stuinfo CHANGE id stu_id INT;				#修改id字段为stu_id字段

mysql> DESC stuinfo;		#id字段改为stu_id字段

#删除字段
mysql> ALTER TABLE stuinfo DROP class;							#删除class字段

mysql> DESC stuinfo;		#确认class字段被删除

#重命名表
mysql> SHOW TABLES;									#查看当前库下所有表

mysql> ALTER TABLE stuinfo RENAME students_info;	#修改表名，不影响表结构和已有数据

mysql> SHOW TABLES;									#查看前期库下所有表

mysql> DESC students_info;							#查看表结构是否变化
```

##### 复制表

```mysql
##语法格式：
	#语法1：复制表结构及表数据，不复制key
		CREATE TABLE 库名.表名  SELECT 字段列表 FROM 库名.表名;
	#语法2：复制表结构和key
		CREATE TABLE 库名.表名  LIKE 库名.表名;
		
#复制表练习
mysql> DESC tarena.user;									#确认原表结构

mysql> SELECT * FROM tarena.user;							#确认tarena.user有数据

mysql> CREATE TABLE studb.user1 SELECT * FROM tarena.user;	#复制tarena.user表所有字段

mysql> SHOW TABLES;											#查看当前库下表

mysql> DESC studb.user1;									#查看复制表user1表结构(无key)

mysql> SELECT * FROM studb.user1;							#确认数据复制成功

mysql> CREATE TABLE studb.user2 LIKE tarena.user;			#复制tarena.user表结构

mysql> SHOW TABLES;											#查看当前库下表

mysql> DESC studb.user2;									#查看复制表user2表结构(有key)

mysql> SELECT * FROM studb.user2;							#确认未复制复制
```

##### 管理表记录(表记录增删改查)

```mysql
##语法：	
	#查
		SELECT 字段列表 FROM 库名.表名 WHERE 筛选条件;
	#增
		INSERT INTO 库名.表名(字段列表) VALUES (值列表1),(值列表2)...;
	#改
		UPDATE 库名.表名 SET 字段=值,字段=值... WHERE 条件筛选;
	#删
		DELETE FROM 库名.表名 WHERE 条件筛选
		
#管理表记录练习
mysql> USE studb;								#使用studb库

mysql> SHOW TABLES;								#查看当前库下表

mysql> ALTER TABLE students_info RENAME info;	#修改表名便于后续练习

#INSERT INTO语句(值要与字段类型匹配)
mysql> INSERT INTO info(stu_id,name) VALUES (1,'zhangsan');				#指定字段增1条

mysql> SELECT * FROM studb.info;

mysql> INSERT INTO info(stu_id,name) VALUES (2,'lisi'),(3,'wangwu');	#指定字段增多条

mysql> SELECT * FROM studb.info;

mysql> INSERT INTO info(stu_id,name,age,gender,phone) VALUES 	(4,'xiaoming',15,'male','13867895432');									#全字段增1条

mysql> SELECT * FROM studb.info;

mysql> INSERT INTO info VALUES (5,'bob',18,'male','17712345678'),	(6,'lily',16,'girl','18534567821');										#全字段增多条

mysql> SELECT * FROM studb.info;

mysql> INSERT INTO studb.info SET name='wangdachui',gender='male';		#SET赋值

mysql> SELECT * FROM studb.info;

mysql> INSERT INTO studb.info(name) SELECT name FROM tarena.user WHERE uid<3;	#SELECT赋值

mysql> SELECT * FROM studb.info;

#UPDATE语句(不写WHERE表示修改所有表记录)
mysql> UPDATE studb.info SET age=18,gender='boy' 
	-> WHERE age IS NULL AND gender IS NULL;					#多字段修改符合条件记录

mysql> SELECT * FROM studb.info;

#DELETE语句(不写WHERE表示删除所有表记录)
mysql> DELETE FROM studb.info WHERE phone IS NULL;				#删除符合条件的表记录

mysql> SELECT * FROM studb.info;
```

#### MySQL数据类型

##### 字符串类型

```mysql
#常用字符串类型练习

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'
mysql> SHOW DATABASES;

mysql> USE studb;

mysql> CREATE TABLE t1 (name CHAR(5), email VARCHAR(10));	#创建t1表测试CHAR和VARCHAR

mysql> DESC t1;

mysql> INSERT INTO t1 VALUES ('zs','zs@tedu.cn');			#两字段都满足，写入成功

mysql> INSERT INTO t1 VALUES ('zhsan','zs@tedu.cn');		#两字段都满足，写入成功

mysql> INSERT INTO t1 VALUES ('zhangsan','zs@tedu.cn');		#name字段超出，写入失败
ERROR 1406 (22001): Data too long for column 'name' at row 1

mysql> INSERT INTO t1 VALUES ('zhsan','zhsan@tedu.cn');		#email字段超出，写入失败
ERROR 1406 (22001): Data too long for column 'email' at row 1

mysql> INSERT INTO t1 VALUES ('张三','zs@tedu.cn');	   	   #中文写入失败，字符编码问题
ERROR 1366 (HY000): Incorrect string value: '\xE5\xBC\xA0\xE4\xB8\x89' for column 'name' at row 1

mysql> SHOW CREATE TABLE t1;								#查看创建t1表完整语句

mysql> CREATE TABLE t2 (name CHAR(5), email VARCHAR(10)) ENGINE=InnoDB DEFAULT CHARSET=utf8;											   #创建t2表测试中文存储
	
mysql> SHOW CREATE TABLE t2;								#查看创建t2表完整语句

mysql> INSERT INTO t2 VALUES ('张三','zs@tedu.cn');		   #测试中文存储

mysql> SELECT * FROM t2;
```

##### 数值类型

```mysql
#测试数值类型
mysql> CREATE TABLE t3(id TINYINT UNSIGNED, money DOUBLE);	#创建t3表测试TINYINT和DOUBLE

mysql> DESC t3;

mysql> INSERT INTO t3 VALUES (1,10.25);						#两字段符合要求，写入成功

mysql> INSERT INTO t3 VALUES (2,-100.368);					#两字段符合要求，写入成功

mysql> INSERT INTO t3 VALUES (255,200);						#两字段符合要求，写入成功

mysql> INSERT INTO t3 VALUES (0,100);						#两字段符合要求，写入成功

mysql> INSERT INTO t3 VALUES (256,100);						#id字段超出范围，写入失败
ERROR 1264 (22003): Out of range value for column 'id' at row 1

mysql> INSERT INTO t3 VALUES (-1,100);						#id字段超出范围，写入失败
ERROR 1264 (22003): Out of range value for column 'id' at row 1

mysql> SELECT * FROM t3;

mysql> INSERT INTO t3 VALUES (1.1,2.2);						#测试整形存储小数

mysql> INSERT INTO t3 VALUES (1.6,2.2);						#存储成功，四舍五入

mysql> SELECT * FROM t3;
```

##### 枚举类型

```mysql
#测试枚举类型
mysql> CREATE TABLE t4(name CHAR(10), gender ENUM('male','female'), likes SET('eat','sleep','study'));										#创建t4表测试ENUM和SET

mysql> DESC t4;

mysql> INSERT INTO t4 VALUES ('zhangsan','male','eat,sleep');	#三字段满足，写入成功

mysql> INSERT INTO t4 VALUES ('lisi','man','eat,sleep');	#gender字段超范围，写入失败
ERROR 1265 (01000): Data truncated for column 'gender' at row 1

mysql> INSERT INTO t4 VALUES ('lisi','male','eat,play');	#likes字段超范围，写入失败
ERROR 1265 (01000): Data truncated for column 'likes' at row 1

mysql> INSERT INTO t4 VALUES ('lisi','male','study');		#三字段满足，写入成功

mysql> SELECT * FROM t4;
```

##### 日期时间类型

```mysql
#测试日期时间类型
mysql> CREATE TABLE t5( name CHAR(10), birth_year YEAR, hire_date DATE, record_time TIME, meeting DATETIME, party TIMESTAMP);					#创建t5表测试日期时间相关

mysql> DESC t5;

mysql> INSERT INTO t5 VALUES  ('xiaoming',1995,20230409,084536,20230510143000,20230510223045);		  #写入成功

mysql> SELECT * FROM t5;

mysql> INSERT INTO t5(name,birth_year) VALUES ('bob',69),('tom',70);	#测试赋值

mysql> SELECT * FROM t5;

mysql> INSERT INTO t5 VALUES ('xiaohong',YEAR(20000101),CURDATE(),CURTIME(),NOW(),NOW());			#测试时间函数赋值

mysql> SELECT * FROM t5;
```

