### RDBMS1-DAY01-笔记

***

#### 环境准备

##### 环境要求

- 所有主机需配置好主机名
- 所有主机配置好IP地址
- 所有主机配置好yum源
- 所有主机关闭防火墙和SELinux

##### 环境规划

| 主机名    | IP地址            | 角色         |
| --------- | ----------------- | ------------ |
| pubserver | 192.168.88.240/24 | yum源服务器  |
| server50  | 192.168.88.50/24  | 数据库客户机 |
| server51  | 192.168.88.51/24  | 数据库服务器 |
| server52  | 192.168.88.52/24  | 数据库服务器 |

```shell
#准备pubserver
[root@pubserver ~]# hostname
pubserver
[root@pubserver ~]# ifconfig eth0 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.88.240  netmask 255.255.255.0  broadcast 192.168.88.255
[root@pubserver ~]# systemctl disable firewalld --now
[root@pubserver ~]# getenforce 
Disabled
[root@pubserver ~]# 

#准备server50
[root@server50 ~]# hostname
server50
[root@server50 ~]# ifconfig eth0 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.88.50  netmask 255.255.255.0  broadcast 192.168.88.255
[root@server50 ~]# systemctl disable firewalld --now
[root@server50 ~]# getenforce 
Disabled
[root@server50 ~]# 

#准备server51
[root@server51 ~]# hostname
server51
[root@server51 ~]# ifconfig eth0 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.88.51  netmask 255.255.255.0  broadcast 192.168.88.255
[root@server51 ~]# systemctl disable firewalld --now
[root@server51 ~]# getenforce 
Disabled
[root@server51 ~]# 

#准备server52
[root@server52 ~]# hostname
server52
[root@pubserver ~]# ifconfig eth0 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.88.52  netmask 255.255.255.0  broadcast 192.168.88.255
[root@server52 ~]# systemctl disable firewalld --now
[root@server52 ~]# getenforce 
Disabled
[root@server52 ~]# 
```

##### 构建系统yum源(如果保留了之前的机器则无需操作)

```shell
##确保虚拟光驱装载ISO文件！！

#搭建临时yum源
[root@pubserver ~]# mkdir /dvd
[root@pubserver ~]# mount /dev/cdrom /dvd/
mount: /dev/sr0 is write-protected, mounting read-only
[root@pubserver ~]# ls /mnt/
CentOS_BuildTag  EFI  EULA  GPL  images  isolinux  LiveOS  Packages  repodata  RPM-GPG-KEY-CentOS-7  RPM-GPG-KEY-CentOS-Testing-7  TRANS.TBL
[root@pubserver ~]# rm -rf /etc/yum.repos.d/*
[root@pubserver ~]# vim /etc/yum.repos.d/development.repo 
[root@pubserver ~]# cat /etc/yum.repos.d/development.repo 
[development]
name=centos7-$releasever
enabled=1
gpgcheck=0
baseurl=file:///dvd/
[root@pubserver ~]# yum clean all; yum repolist
...
repolist: 10,072
[root@pubserver ~]# 

#安装vsftpd软件
[root@pubserver ~]# yum -y install vsftpd

#启动vsftpd服务
[root@pubserver ~]# systemctl enable vsftpd.service --now
[root@pubserver ~]# ss -anptul | grep vsftpd
tcp    LISTEN     0      32       :::21                   :::*                   users:(("vsftpd",pid=1142,fd=3))
[root@pubserver ~]# 

#调整yum配置，使用vsftpd服务发布
[root@pubserver ~]# umount /dvd/
[root@pubserver ~]# mkdir /var/ftp/centos
[root@pubserver ~]# vim /etc/fstab 
[root@pubserver ~]# tail -1 /etc/fstab 
/dev/cdrom /var/ftp/centos/ iso9660 defaults 0 0
[root@pubserver ~]# mount -a
mount: /dev/sr0 is write-protected, mounting read-only
[root@pubserver ~]# ls /var/ftp/centos/
CentOS_BuildTag  EFI  EULA  GPL  images  isolinux  LiveOS  Packages  repodata  RPM-GPG-KEY-CentOS-7  RPM-GPG-KEY-CentOS-Testing-7  TRANS.TBL
[root@pubserver ~]# vim /etc/yum.repos.d/development.repo 
[root@pubserver ~]# cat /etc/yum.repos.d/development.repo 
[development]
name=centos7-$releasever
enabled=1
gpgcheck=0
baseurl=ftp://192.168.88.240/centos/
[root@pubserver ~]# yum clean all; yum repolist
...
repolist: 10,072
[root@pubserver ~]# 
```

##### 自定义MySQL软件相关yum源

```shell
#上传MySQL软件至yum源服务器
[root@pubserver ~]# ls mysql-5.7.17.tar 
mysql-5.7.17.tar

#vsftpd服务家目录下创建MySQL软件发布目录
[root@pubserver ~]# mkdir /var/ftp/mysqlsofts
[root@pubserver ~]# tar -xf mysql-5.7.17.tar -C /var/ftp/mysqlsofts/
[root@pubserver ~]# ls /var/ftp/mysqlsofts/
mysql-community-client-5.7.17-1.el7.x86_64.rpm           mysql-community-libs-5.7.17-1.el7.x86_64.rpm
mysql-community-common-5.7.17-1.el7.x86_64.rpm           mysql-community-libs-compat-5.7.17-1.el7.x86_64.rpm
mysql-community-devel-5.7.17-1.el7.x86_64.rpm            mysql-community-minimal-debuginfo-5.7.17-1.el7.x86_64.rpm
mysql-community-embedded-5.7.17-1.el7.x86_64.rpm         mysql-community-server-5.7.17-1.el7.x86_64.rpm
mysql-community-embedded-compat-5.7.17-1.el7.x86_64.rpm  mysql-community-test-5.7.17-1.el7.x86_64.rpm
mysql-community-embedded-devel-5.7.17-1.el7.x86_64.rpm
[root@pubserver ~]# 

#安装createrepo命令用于自定义yum源
[root@pubserver ~]# yum -y install createrepo

#自定义MySQL软件yum源
[root@pubserver ~]# cd /var/ftp/mysqlsofts/
[root@pubserver mysqlsofts]# createrepo ./
[root@pubserver mysqlsofts]# ls repodata/
61c1afd2816e9cb246898f73cf681ec609dc5b967b53b8b85feb412bb8418f15-filelists.xml.gz
8d25df9750701e50efe414440978c82c88c0de63f4b4fe0e1f38acc9cd56def2-filelists.sqlite.bz2
9a80867a8e5901b0894ab8c199a48afde2344420942cd50efde32c40ab0ab6e5-primary.sqlite.bz2
c656859772a42faaef69e58a006282ddc196a1fe891d4c0cca004c31027a0b66-other.sqlite.bz2
cb0aea53d4885306fb466ee34f636583c0c4f3f3d38662aab8eaef08b35e8d0f-other.xml.gz
f030d48e745aa2de31ee12769f646b492e4b9a352725b048902492a9626caf79-primary.xml.gz
repomd.xml
[root@pubserver mysqlsofts]# cd
[root@pubserver ~]# 

#编写repo文件
[root@pubserver ~]# vim /etc/yum.repos.d/mysql.repo
[root@pubserver ~]# cat /etc/yum.repos.d/mysql.repo 
[mysql_softs]
name=mysql_softs
enabled=1
gpgcheck=0
baseurl=ftp://192.168.88.240/mysqlsofts/
[root@pubserver ~]# yum clean all; yum repolist 
...
repolist: 10,083											#注意相比之前多了11个软件包
[root@pubserver ~]# 
```

#### 搭建数据库服务器

##### 构建第一台数据库服务器

```shell
#确认MariaDB软件未安装
[root@server51 ~]# rpm -q maraidb
package maraidb is not installed
[root@server51 ~]# rpm -q maraidb-server
package maraidb-server is not installed
[root@server51 ~]# 

#同步Yum源服务器上的repo文件
[root@server51 ~]# rm -rf /etc/yum.repos.d/*
[root@server51 ~]# scp 192.168.88.240:/etc/yum.repos.d/*.repo /etc/yum.repos.d/
[root@server51 ~]# ls /etc/yum.repos.d/
development.repo  mysql.repo
[root@server51 ~]# 

#确认Yum可用
[root@server51 ~]# yum clean all; yum repolist
...
repolist: 10,083
[root@server51 ~]# 

#安装MySQL相关软件
[root@server51 ~]# yum -y install mysql-community-*

#启动MySQL服务
[root@server51 ~]# systemctl enable mysqld
[root@server51 ~]# systemctl start mysqld
[root@server51 ~]# ss -anptul | grep mysql
tcp    LISTEN     0      80       :::3306                 :::*                   users:(("mysqld",pid=2026,fd=19))
[root@server51 ~]# 
```

##### 登录MySQL服务

```shell
##相比于MariaDB，MySQL-5.7.17版本对于root用户有初始随机密码，首次登录需使用随机密码
#获取MySQL随机密码(随机密码中可能会出现特殊字符并且每个人的随机密码都不一样)
[root@server51 ~]# ls /var/log/mysqld.log 
/var/log/mysqld.log
[root@server51 ~]# cat /var/log/mysqld.log | grep password
2023-05-10T05:53:47.152477Z 1 [Note] A temporary password is generated for root@localhost: R4q,TY/Jee;;
[root@server51 ~]# 

#登录MySQL服务
#登录语法: mysql -h数据库服务器地址 -P数据库服务端口 -u登录数据库用户 -p登陆数据库用户密码
[root@server51 ~]# mysql -hlocalhost -P3306 -uroot -p'R4q,TY/Jee;;'
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.17

Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW DATABASES;			#使用随机密码登录后无法执行任何操作，首次登录必须修改密码
ERROR 1820 (HY000): You must reset your password using ALTER USER statement before executing this statement.
mysql> ALTER USER root@'localhost' IDENTIFIED BY '123qqq...A';	
Query OK, 0 rows affected (0.00 sec)

mysql> SHOW DATABASES;			#修改root用户密码之后可执行SQL命令
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.00 sec)

mysql> exit
Bye
[root@server51 ~]# 
```

##### 数据库基本操作

```mysql
#登录数据库服务执行常用SQL命令
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

#进入SQL命令行
mysql> STATUS;				/*查看MySQL服务状态*/
--------------
mysql  Ver 14.14 Distrib 5.7.17, for Linux (x86_64) using  EditLine wrapper

Connection id:          4
Current database:
Current user:           root@localhost
SSL:                    Not in use
Current pager:          stdout
Using outfile:          ''
Using delimiter:        ;
Server version:         5.7.17 MySQL Community Server (GPL)
Protocol version:       10
Connection:             Localhost via UNIX socket
Server characterset:    latin1
Db     characterset:    latin1
Client characterset:    utf8
Conn.  characterset:    utf8
UNIX socket:            /var/lib/mysql/mysql.sock
Uptime:                 1 hour 44 min 13 sec

Threads: 1  Questions: 11  Slow queries: 0  Opens: 106  Flush tables: 1  Open tables: 99  Queries per second avg: 0.001
--------------

mysql> SELECT USER();		/*查看当前登录用户*/
+----------------+
| USER()         |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)

mysql> SELECT VERSION();	/*查看当前MySQL服务版本*/
+-----------+
| VERSION() |
+-----------+
| 5.7.17    |
+-----------+
1 row in set (0.00 sec)

mysql> SELECT DATABASE();	/*查看当前正在操作的库*/
+------------+
| DATABASE() |
+------------+
| NULL       |				/*NULL表示并未进入任何库*/
+------------+
1 row in set (0.00 sec)

mysql> SHOW DATABASES;		/*查看当前已有库*/
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.00 sec)

mysql> SHOW TABLES FROM sys;	/*查看指定库下所有表*/
+-----------------------------------------------+
| Tables_in_sys                                 |
+-----------------------------------------------+
| host_summary                                  |
| host_summary_by_file_io                       |
|...											|
| x$waits_global_by_latency                     |
+-----------------------------------------------+
101 rows in set (0.00 sec)

mysql> USE sys;					/*切换至指定库*/
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

mysql> SELECT DATABASE();		/*查看当前正在操作的库*/
+------------+
| DATABASE() |
+------------+
| sys        |
+------------+
1 row in set (0.00 sec)
Database changed

mysql> SHOW TABLES;				/*查看当前操作库下所有表*/
+-----------------------------------------------+
| Tables_in_sys                                 |
+-----------------------------------------------+
| host_summary                                  |
| ...                 |
| x$waits_global_by_latency                     |
+-----------------------------------------------+
101 rows in set (0.00 sec)
	
mysql> exit						/*退出SQL命令行，回到Linux命令行*/
Bye
[root@server51 ~]# 
```

##### 构建第二台数据库服务器

```shell
#同步repo文件
[root@server52 ~]# rm -rf /etc/yum.repos.d/*
[root@server52 ~]# scp 192.168.88.240:/etc/yum.repos.d/*.repo /etc/yum.repos.d/ 
[root@server52 ~]# ls /etc/yum.repos.d/
development.repo  mysql.repo

#确认Yum可用
[root@server52 ~]# yum clean all; yum repolist
...
repolist: 10,083
[root@server52 ~]# 

#安装MySQL服务
[root@server52 ~]# yum -y install mysql-community-*

#启动MySQL服务
[root@server52 ~]# systemctl enable mysqld
[root@server52 ~]# systemctl start mysqld
[root@server52 ~]# ss -anptul | grep mysql
tcp    LISTEN     0      80       :::3306                 :::*                   users:(("mysqld",pid=2045,fd=22))
[root@server52 ~]# 

#获取MySQL初始密码
[root@server52 ~]# cat /var/log/mysqld.log | grep password
2023-05-10T07:49:42.563909Z 1 [Note] A temporary password is generated for root@localhost: 4kVj)7/%k*j1
[root@server52 ~]# 

#登录MySQL并修改root用户密码为"NSD2023...a"
[root@server52 ~]# mysql -hlocalhost -uroot -p'4kVj)7/%k*j1'

mysql> ALTER USER root@'localhost' IDENTIFIED BY 'NSD2023...a';
Query OK, 0 rows affected (0.00 sec)

mysql> exit
Bye
[root@server52 ~]# 
```

#### MySQL密码管理

##### 修改已知密码

- 登录后修改

```shell
##思路：登录到MySQL命令行使用ALTER USER语句修改密码

#登录MySQL服务
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

#执行SQL命令修改密码
mysql> ALTER USER root@'localhost' IDENTIFIED BY 'NSD2023...a';
Query OK, 0 rows affected (0.00 sec)

mysql> EXIT
Bye

#测试新旧密码
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'		#旧密码无法登录
[root@server51 ~]# mysql -hlocalhost -uroot -p'NSD2023...a'		#新密码可以登录
```

- 登录前修改

```shell
##思路：使用mysqladmin命令在Linux命令行修改MySQL用户密码
#语法：
#	1）mysqladmin -hlocalhost -u用户 -p"旧密码" password "新密码"
#	2）mysqladmin -hlocalhost -u用户 -p password

#测试语法1
[root@server51 ~]# mysqladmin -hlocalhost -uroot -p'NSD2023...a' password "123qqq...A"
[root@server51 ~]# mysql -hlocalhost -uroot -p'NSD2023...a'		#旧密码无法登录
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'		#新密码可以登录

#测试语法2(推荐)
[root@server51 ~]# mysqladmin -hlocalhost -uroot -p password
Enter password: 		#输入旧密码123qqq...A，无提示符，确认正确后回车
New password: 			#输入新密码NSD2023...a
Confirm new password: 	#确认新密码NSD2023...a
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'		#旧密码无法登录
[root@server51 ~]# mysql -hlocalhost -uroot -p'NSD2023...a'		#新密码可以登录
```

##### 破解未知密码

- 允许重启服务

```shell
##思路：MySQL的用户与密码以表记录形式存储，通过修改表记录实现破解密码

#了解MySQL用户及密码存储情况
[root@server51 ~]# mysql -hlocalhost -uroot -p'NSD2023...a'
mysql> SHOW TABLES FROM mysql;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| columns_priv              |
|...						|
| user                      |				#MySQL用户信息记录在mysql.user表中
+---------------------------+
31 rows in set (0.00 sec)

#查看mysql.user表结构
mysql> DESC mysql.user;						

#查看mysql.user表中符合条件的表记录
mysql> SELECT 						
	-> user,host,authentication_string 
	-> FROM mysql.user WHERE user='root';
+------+-----------+-------------------------------------------+
| user | host      | authentication_string                     |
+------+-----------+-------------------------------------------+
| root | localhost | *28FA516E8DA40768CBADCFC72E7A6CD4ADA15D1E |
+------+-----------+-------------------------------------------+
1 row in set (0.00 sec)

mysql> exit
Bye
[root@server51 ~]# 

##破解密码(skip_grant_tables)
#修改/etc/my.cnf并重启服务
[root@server51 ~]# ls /etc/my.cnf				#MySQL主配置文件
/etc/my.cnf
[root@server51 ~]# vim /etc/my.cnf				#编辑配置文件
[root@server51 ~]# sed -rn '4,5p' /etc/my.cnf	#新增内容，注意不是删除其他内容！！
[mysqld]
skip_grant_tables								#跳过权限认证表格校验
[root@server51 ~]# systemctl restart mysqld		#服务正常重启表示配置文件内容正确
[root@server51 ~]# 

#登录MySQL服务(无密码)
[root@server51 ~]# mysql

#使用UPDATE语句修改表记录(修改mysql.user表中root用户的authentication_string字段值)
mysql> UPDATE mysql.user SET authentication_string=PASSWORD('123qqq...A')
    -> WHERE
    -> user='root' AND host='localhost';
Query OK, 1 row affected, 1 warning (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 1

#刷新授权确保修改生效
mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)


mysql> SELECT user,host,authentication_string FROM mysql.user
    -> WHERE
    -> user='root' AND host='localhost';
+------+-----------+-------------------------------------------+
| user | host      | authentication_string                     |
+------+-----------+-------------------------------------------+
| root | localhost | *F19C699342FA5C91EBCF8E0182FB71470EB2AF30 |
+------+-----------+-------------------------------------------+
1 row in set (0.00 sec)

mysql> exit
Bye
[root@server51 ~]# 

#修改配置文件并重启服务
[root@server51 ~]# vim /etc/my.cnf
[root@server51 ~]# sed -rn '4,5p' /etc/my.cnf
[mysqld]
#skip_grant_tables								#注释或删除掉skip_grant_tables语句
[root@server51 ~]# systemctl restart mysqld

#确认新密码登录
[root@server51 ~]# mysql										#空密码无法登录
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'		#使用破解密码可登录
```

- 不允许重启服务

```shell
##思路：数据以文件形式存储在文件系统上，用已知密码的库文件覆盖未知密码的库文件实现破解

#本实验在server52操作，注意不要操作错机器
#查看数据库文件存储情况
[root@server52 ~]# cat /etc/my.cnf | grep "datadir"		#获取数据库数据存储目录
datadir=/var/lib/mysql
[root@server52 ~]# ls /var/lib/mysql					#查看目录下的文件
auto.cnf    ca.pem           client-key.pem  ibdata1      ib_logfile1  mysql       mysql.sock.lock     private_key.pem  server-cert.pem  sys
ca-key.pem  client-cert.pem  ib_buffer_pool  ib_logfile0  ibtmp1       mysql.sock  performance_schema  public_key.pem   server-key.pem
[root@server52 ~]# ls /var/lib/mysql/mysql/user.*		#查看mysql.user表相关文件
/var/lib/mysql/mysql/user.frm  /var/lib/mysql/mysql/user.MYD  /var/lib/mysql/mysql/user.MYI
[root@server52 ~]# 

#覆盖/var/lib/mysql/mysql/目录
[root@server52 ~]# scp -r 192.168.88.51:/var/lib/mysql/mysql /var/lib/mysql

#查找server52上MySQL主进程号
[root@server52 ~]# ps aux | grep -v "grep" | grep mysql
mysql      2045  0.0 17.2 1119528 171856 ?      Sl   15:49   0:00 /usr/sbin/mysqld --daemonize --pid-file=/var/run/mysqld/mysqld.pid
[root@server52 ~]# 

#热加载MySQL相关文件
[root@server52 ~]# kill -l				#查看信号向量列表(小写的l字母，不是数字1！！)
[root@server52 ~]# kill -SIGHUP 2045	#调用SIGHUP向量实现MySQL服务热加载

#使用server51上已知的密码测试登录
[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A'
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 4
Server version: 5.7.17 MySQL Community Server (GPL)

Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> exit
Bye
[root@server52 ~]# 
```

#### MySQL图形管理工具

##### 网页工具PHPMyAdmin

- server51部署LAP环境

```shell
#再次确认server51上防火墙和SELinux已关闭
[root@server51 ~]# systemctl status firewalld
[root@server51 ~]# getenforce 
Disabled
[root@server51 ~]# 

#安装httpd、php和php-mysql
#	httpd提供网站服务
#	php用于解析PHP代码
#	php-mysql用于连接MySQL服务获取数据
[root@server51 ~]# yum clean all; yum repolist
...
repolist: 9,922
[root@server51 ~]# yum -y install httpd php php-mysql

#启动httpd服务
[root@server51 ~]# systemctl enable httpd.service 
[root@server51 ~]# systemctl start httpd.service 
[root@server51 ~]# ss -antupl | grep :80
tcp    LISTEN     0      128      :::80                   :::*                   users:(("httpd",pid=3020,fd=4),("httpd",pid=3019,fd=4),("httpd",pid=3018,fd=4),("httpd",pid=3017,fd=4),("httpd",pid=3016,fd=4),("httpd",pid=3015,fd=4))
[root@server51 ~]# 

#测试LNP环境
[root@server51 ~]# vim /var/www/html/test.php		#编写PHP测试页面
[root@server51 ~]# cat /var/www/html/test.php 
<?PHP
    $i=33;
    echo $i;
?>
[root@server51 ~]# curl http://localhost/test.php	#访问测试，见到33则成功
33[root@server51 ~]# 

#上传PHPMyAdmin软件包
[root@server51 ~]# ls phpMyAdmin-2.11.11-all-languages.tar.gz 
phpMyAdmin-2.11.11-all-languages.tar.gz

#解压PHPMyAdmin包
[root@server51 ~]# tar -xf phpMyAdmin-2.11.11-all-languages.tar.gz 

#部署PHP页面到httpd网页家目录
[root@server51 ~]# mv phpMyAdmin-2.11.11-all-languages /var/www/html/phpmyadmin/

#配置PHPMyAdmin
[root@server51 ~]# cd /var/www/html/phpmyadmin/
[root@server51 phpmyadmin]# cp config.sample.inc.php config.inc.php
[root@server51 phpmyadmin]# vim config.inc.php 				
[root@server51 phpmyadmin]# sed -rn '17p' config.inc.php 
$cfg['blowfish_secret'] = '123456'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */
[root@server51 phpmyadmin]# cd
[root@server51 ~]# 。

#打开浏览器访问测试
#	http://192.168.88.51/phpmyadmin/
#	用户和密码是数据库服务的用户和密码
```

![1683709297257](RDBMS01-DAY01.assets/1683709297257.png)

![1683709423141](RDBMS01-DAY01.assets/1683709423141.png)

##### 图形工具MySQLWorkBench

![1683709789500](RDBMS01-DAY01.assets/1683709789500.png)

#### 基础查询语句

##### 基础语法

```mysql
##语法格式
#	1）
	SELECT 字段列表 FROM 库名.表名;
#	2)
	SELECT 字段列表 FROM 库名.表名 WHERE 筛选条件;
#	3)
	SELECT "abc";		#常量
	SELECT @@VERSION;	#变量
	SELECT 2+3;			#计算表达式
	SELECT USER();		#函数
	
#结论：字段列表决定查询结果宽度，筛选条件决定查询结果长度
```

##### 语法规范

```mysql
##语法规范

#\c终止输入(输入发生错误的时候使用)
mysql> SHOW \c

#单行注释使用#或--
mysql> # SHOW DATABASES;
mysql> -- SHOW DATABASES;

#多行注释使用/**/
mysql> /* SELECT
   /*> user,host
   /*> FROM
   /*> mysql.user;
   /*> */
mysql> 

#默认不支持tab补全(可以通过一些插件解决)

#SQL语句默认以;结尾或\G结尾(;以行的形式显示结果，\G以列的形式显示结果)
mysql> SELECT USER();
+----------------+
| USER()         |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)

mysql> SELECT USER()\G
*************************** 1. row ***************************
USER(): root@localhost
1 row in set (0.00 sec)

mysql> 

#可根据需要缩进或换行
mysql> SELECT user,host FROM mysql.user;
mysql> SELECT 
    -> user,host 
    -> FROM
    -> mysql.user;
    
#SQL命令不区分大小写(字段名、变量名除外)
```

##### 导入练习数据

```shell
##server51操作！

#上传tarena.sql文件
[root@server51 ~]# ls tarena.sql 
tarena.sql

#将sql文件内容导入到数据库(执行1次即可！！)
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' < tarena.sql

#登录MySQL服务
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

mysql> SHOW DATABASES;				#确认多一个tarena库
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| tarena             |
+--------------------+
5 rows in set (0.00 sec)

mysql> SHOW TABLES FROM tarena;		#tarena库下有4张表用于后续实验
+------------------+
| Tables_in_tarena |
+------------------+
| departments      |
| employees        |
| salary           |
| user             |
+------------------+
4 rows in set (0.00 sec)
```

##### 查询语句基础练习

```mysql
#基础查询练习
mysql> USE tarena;					#使用tarena库
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> SHOW TABLES;					#查看tarena库下所有表
+------------------+
| Tables_in_tarena |
+------------------+
| departments      |
| employees        |
| salary           |
| user             |
+------------------+
4 rows in set (0.00 sec)

mysql> DESC user;					#查看user表的表结构(user表内容为/etc/passwd文件内容)
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| name     | char(20)    | YES  |     | NULL    |                |
| password | char(1)     | YES  |     | NULL    |                |
| uid      | int(11)     | YES  |     | NULL    |                |
| gid      | int(11)     | YES  |     | NULL    |                |
| comment  | varchar(50) | YES  |     | NULL    |                |
| homedir  | varchar(80) | YES  |     | NULL    |                |
| shell    | char(30)    | YES  |     | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
8 rows in set (0.00 sec)

mysql> SELECT name FROM tarena.user;					#查询表内单个字段所有值

mysql> SELECT name,uid,shell FROM tarena.user;			#查询表内多个字段所有值

mysql> SELECT 
	-> id,name,password,uid,gid,comment,homedir,shell 
	-> FROM 
	-> tarena.user;										#查询表内所有字段所有值

mysql> SELECT * FROM tarena.user;						#查询表内所有字段所有值

mysql> SELECT * FROM user 
    -> WHERE name='root';								#查询表内所有字段符合条件的值
+----+------+----------+------+------+---------+---------+-----------+
| id | name | password | uid  | gid  | comment | homedir | shell     |
+----+------+----------+------+------+---------+---------+-----------+
|  1 | root | x        |    0 |    0 | root    | /root   | /bin/bash |
+----+------+----------+------+------+---------+---------+-----------+
1 row in set (0.00 sec)

mysql> SELECT 
    -> name,uid,shell
    -> FROM user
    -> WHERE
    -> name='root';										#查询表内指定字段符合条件的值
+------+------+-----------+
| name | uid  | shell     |
+------+------+-----------+
| root |    0 | /bin/bash |
+------+------+-----------+
1 row in set (0.00 sec)
```

##### 查询语句补充练习

```mysql
#SELECT补充用法
mysql> SELECT "abc";				#直接打印字符
+-----+
| abc |
+-----+
| abc |
+-----+
1 row in set (0.00 sec)

mysql> SELECT 8;					#直接打印数字
+---+
| 8 |
+---+
| 8 |
+---+
1 row in set (0.00 sec)

mysql> SHOW VARIABLES;				#查看MySQL所有变量

mysql> SELECT @@VERSION;			#查询指定变量
+-----------+
| @@VERSION |
+-----------+
| 5.7.17    |
+-----------+
1 row in set (0.00 sec)

mysql> SELECT 2+3;					#查询计算表达式结果
+-----+
| 2+3 |
+-----+
|   5 |
+-----+
1 row in set (0.00 sec)

mysql> SELECT NOW();				#查询函数
+---------------------+
| NOW()               |
+---------------------+
| 2023-05-10 18:02:20 |
+---------------------+
1 row in set (0.00 sec)
```

##### 条件筛选

- 数字比较

| 符号 | =    | !=     | <    | <=       | >    | >=       |
| ---- | ---- | ------ | ---- | -------- | ---- | -------- |
| 含义 | 等于 | 不等于 | 小于 | 小于等于 | 大于 | 大于等于 |

```mysql
#数字比较练习
mysql> SELECT id,name,shell FROM tarena.user WHERE id=1;	#查询id等于1的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE id!=1;	#查询id不等于1的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE id<3;	#查询id小于3的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE id<=3;	#查询id小于等于3的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE id>23;	#查询id大于23的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE id>=23;	#查询id大于等于23的表记录
```

- 字符比较

| 符号 | =    | !=     |
| ---- | ---- | ------ |
| 含义 | 等于 | 不等于 |

```mysql
#字符比较练习(字符串需要用引号引起来)
mysql> SELECT id,name,shell FROM tarena.user WHERE name='root';			#查询用户名是root的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE shell!="/bin/bash";	#查询解释器不是/bin/bash的表记录
```

- 空与非空

| 符号 | IS NULL    | IS NOT NULL  |
| ---- | ---------- | ------------ |
| 含义 | 字段值为空 | 字段值不为空 |

```mysql
#空与非空匹配练习
mysql> SELECT id,name,shell FROM tarena.user WHERE shell IS NULL;		#查询解释器是空的表记录

mysql> SELECT id,name,shell FROM tarena.user WHERE shell IS NOT NULL;	#查询解释器不是空的表记录

##重点：理解"","NULL",NULL的区别
#	""：为0个字符串
#	"NULL"：为字符串NULL
#	NULL：MySQL中的空，等效于Python中的None

#向tarena.user表写入练习数据
mysql> INSERT INTO tarena.user(id,name) VALUES (101,"");
mysql> INSERT INTO tarena.user(id,name) VALUES (102,"NULL");
mysql> INSERT INTO tarena.user(id,name) VALUES (103,NULL);

#查询测试
mysql> SELECT id,name FROM tarena.user WHERE id>=100;		#确认测试数据已写入
+-----+------+
| id  | name |
+-----+------+
| 101 |      |
| 102 | NULL |
| 103 | NULL |
+-----+------+
3 rows in set (0.00 sec)

mysql> SELECT id,name FROM tarena.user WHERE name="";		#查询用户名为0个字符的表记录
+-----+------+
| id  | name |
+-----+------+
| 101 |      |
+-----+------+
1 row in set (0.00 sec)

mysql> SELECT id,name FROM tarena.user WHERE name="NULL";	#查询用户名为NULL的表记录
+-----+------+
| id  | name |
+-----+------+
| 102 | NULL |
+-----+------+
1 row in set (0.00 sec)

mysql> SELECT id,name FROM tarena.user WHERE name IS NULL;	#查询用户名是空的表记录
+-----+------+
| id  | name |
+-----+------+
| 103 | NULL |
+-----+------+
1 row in set (0.00 sec)
```

- 范围匹配

| 符号 | IN       | NOT IN     | BETWEEN 数字1 AND 数字2 |
| ---- | -------- | ---------- | ----------------------- |
| 含义 | 在范围内 | 不在范围内 | 在数字1和数字2之间      |

```mysql
#范围匹配练习
mysql> SELECT name,uid,shell FROM tarena.user WHERE uid IN (1,3,5);			#查询uid是1或3或5的表记录

mysql> SELECT name,uid,shell FROM tarena.user WHERE shell NOT IN ("/bin/bash","/sbin/nologin");												#查询解释器不是/bin/bash或/sbin/nologin的表记录

mysql> SELECT name,uid,shell FROM tarena.user WHERE uid BETWEEN 10 and 20;	#查询uid是10到20内的表记录
```

- 模糊匹配(关键字：LIKE)

| 符号 | _       | %             |
| ---- | ------- | ------------- |
| 含义 | 1个字符 | 0个或多个字符 |

```mysql
#模糊匹配练习
mysql> SELECT name,shell FROM tarena.user WHERE name LIKE "__";	#查询用户名是两个字符的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name LIKE "a%";	#查询用户名是a开头的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name LIKE "%e";	#查询用户名是e结尾的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name LIKE "%a%";#查询用户名包含a的表记录
```

- 正则查询(关键字REGEXP)

```mysql
#正则匹配练习
mysql> SELECT name,shell FROM tarena.user WHERE name REGEXP "^..$";	#查询用户名是两个字符的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name REGEXP "^a";	#查询用户名是a开头的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name REGEXP "e$";	#查询用户名是e结尾的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name REGEXP "^a|t$";	#查询用户名以a开头或以t结尾的表记录

mysql> SELECT name,shell FROM tarena.user WHERE name REGEXP "a";	#查询用户名包含a的表记录
```

- 提高优先级()

```shell
#()练习
mysql> SELECT 2+3*5;		#先计算乘法
+-------+
| 2+3*5 |
+-------+
|    17 |
+-------+
1 row in set (0.00 sec)

mysql> SELECT (2+3)*5;		#先计算括号
+---------+
| (2+3)*5 |
+---------+
|      25 |
+---------+
1 row in set (0.00 sec)
```

- 逻辑匹配

| 符号 | AND  | OR   | NOT  |
| ---- | ---- | ---- | ---- |
| 符号 | &&   | \|\| | ！   |
| 含义 | 与   | 或   | 非   |

```mysql
#逻辑与(条件都满足)
mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE 
    -> name='root' AND uid=1;					#无结果，用户名是root且uid是1的表记录不存在

mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE
    -> name='root' AND uid=0;					#有结果，用户名是root且uid是0的表记录存在
    
#逻辑或(条件满足任意1个即可)
mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE
    -> name='root' OR uid=1;					#两条表记录，用户名是root或uid是1都符合
    
#逻辑非(取反操作)
mysql> SELECT name,shell FROM tarena.user 
    -> WHERE
    -> shell != "/bin/bash";					#查询解释器不是/bin/bash的表记录
 
mysql> SELECT name,shell FROM tarena.user
    -> WHERE
    -> NOT shell = "/bin/bash";					#对解释器是/bin/bash的表记录取反
    
##重点：逻辑操作符优先级
#()>NOT>AND>OR
mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE
    -> name='root' OR name='bin' AND uid=1;			#先判断AND后判断OR
+------+------+---------------+
| name | uid  | shell         |
+------+------+---------------+
| root |    0 | /bin/bash     |
| bin  |    1 | /sbin/nologin |
+------+------+---------------+

mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE
    -> (name='root' OR name='bin') AND uid=1;		#先判断()内部的OR,后判断AND
+------+------+---------------+
| name | uid  | shell         |
+------+------+---------------+
| bin  |    1 | /sbin/nologin |
+------+------+---------------+
1 row in set (0.00 sec)
```

- 定义别名(关键字AS)

```shell
#定义别名练习
mysql> SELECT 2+3;				#计算2+3表达式

mysql> SELECT 2+3 AS 计算结果;	  #表头名称被改写
	
mysql> SELECT 2+3 计算结果;		  #表头名称被改写

mysql> SELECT name AS 用户名,shell AS 解释器
    -> FROM tarena.user
    -> WHERE name='root';		#字段名被改写
+-----------+-----------+
| 用户名     | 解释器     |
+-----------+-----------+
| root      | /bin/bash |
+-----------+-----------+
1 row in set (0.00 sec)
```

- 数据拼接

```mysql
#CONCAT()函数拼接练习

mysql> SELECT name,shell FROM tarena.user;									#2个字段

mysql> SELECT CONCAT(name,'-',shell) FROM tarena.user;						#1个字段

mysql> SELECT CONCAT(name,'-',uid,'-',shell) FROM tarena.user;				#1个字段

mysql> SELECT CONCAT(name,'-',uid,'-',shell) AS userinfo FROM tarena.user;	#拼接并定义别名
```

- 去重显示(关键字DISTINCT)

```shell
#去重练习
mysql> SELECT DISTINCT shell FROM tarena.user;		#对表中shell字段值进行去重
```

#### 补充知识

##### MySQL-5.7密码策略

```mysql
##server52操作

#登录server52的MySQL服务
[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

#修改为简易密码报错，因为此版本MySQL默认对密码强度有要求
mysql> ALTER USER root@'localhost' IDENTIFIED BY '123456';	#失败
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements
mysql> 

mysql> SHOW VARIABLES LIKE "validate_password%";
| validate_password_length             | 8      |		#密码最短长度
| validate_password_policy             | MEDIUM |		#密码强度级别

mysql> SET GLOBAL validate_password_length=6;			#修改密码最短长度(临时)

mysql> SET GLOBAL validate_password_policy=0;			#修改密码强度级别(临时)

mysql> SHOW VARIABLES LIKE "validate_password%";
+--------------------------------------+-------+
| Variable_name                        | Value |
+--------------------------------------+-------+
| validate_password_length             | 6     |
| validate_password_policy             | LOW   |

mysql> ALTER USER root@'localhost' IDENTIFIED BY '123456';		#成功

mysql> exit
Bye
[root@server52 ~]# mysql -hlocalhost -uroot -p'123456'
mysql> exit
Bye

#实现永久配置生效需写入MySQL主配置文件/etc/my.cnf
[root@server52 ~]# vim /etc/my.cnf								#编辑主配置文件
[root@server52 ~]# sed -rn '4,6p' /etc/my.cnf
[mysqld]
validate_password_policy=0			#加入此行
validate_password_length=6			#加入此行
[root@server52 ~]# systemctl restart mysqld						#重启服务
[root@server52 ~]# mysql -hlocalhost -uroot -p'123456'			#登录MySQL服务

mysql> SHOW VARIABLES LIKE "validate_password%";				#确认变量值
```







