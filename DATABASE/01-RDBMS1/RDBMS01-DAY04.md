### RDBMS1-DAY04-笔记

***

#### 数据批量处理

##### MySQL检索目录

```shell
##server51操作
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW VARIABLES LIKE 'secure_file_priv'"								   #查看MySQL检索目录变量信息
[root@server51 ~]# ll -d /var/lib/mysql-files/		#查看检索目录权限及归属

#如果需要修改MySQL检索目录，则将 secure_file_priv=自定义路径  写入/etc/my.cnf后重启服务即可
```

##### 环境准备

```mysql
#创建新库新表用于后续实验
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'
mysql> CREATE DATABASE execdb;			#创建execdb库

mysql> CREATE TABLE execdb.users(
    -> name CHAR(20),
    -> password CHAR(1),
    -> uid INT,
    -> gid INT,
    -> comment CHAR(200),
    -> homedir CHAR(50),
    -> shell CHAR(20)
    -> );								#创建users表用于存储/etc/passwd文件内容

mysql> DESC execdb.users;				#确认表结构
```

##### 数据导入

```mysql
#语法：
	LOAD DATA INFILE "/目录/文件" INTO TABLE 库名.表名
	FIELDS TERMINATED BY "列分隔符"
	LINES TERMINATED BY "行分隔符";
	
#测试数据导入(表格提前创建，字段数完全对应)
mysql> SYSTEM ls /var/lib/mysql-files/					#SQL命令行下执行Linux命令
mysql> SYSTEM cp /etc/passwd /var/lib/mysql-files/		#拷贝passwd文件到检索目录
mysql> SYSTEM ls /var/lib/mysql-files/
passwd

mysql> LOAD DATA INFILE "/var/lib/mysql-files/passwd" 
    -> INTO TABLE execdb.users
    -> FIELDS TERMINATED BY ":"
    -> LINES TERMINATED BY "\n";						#导入数据

mysql> SELECT * FROM execdb.users;						#确认表内有数据
```



##### 数据导出

```mysql
#语法：
	SELECT 语句 INTO OUTFILE "/目录/文件" 
	FIELDS TERMINATED BY "列分隔符" 
	LINES TERMINATED BY "行分隔符";
	
#测试数据导出(文件无需提前创建)
mysql> SELECT name,uid,shell FROM execdb.users;

#导出数据(默认tab分隔字段，\n换行)
mysql> SELECT name,uid,shell FROM execdb.users INTO OUTFILE "/var/lib/mysql-files/a.txt";					

#导出数据(使用#-#分隔字段，\n换行)
mysql> SELECT name,uid,shell FROM execdb.users INTO OUTFILE "/var/lib/mysql-files/b.txt" FIELDS TERMINATED BY "#-#";

#导出数据(使用#-#分隔字段，!!!分隔行)
mysql> SELECT name,uid,shell FROM execdb.users INTO OUTFILE "/var/lib/mysql-files/c.txt" FIELDS TERMINATED BY "#-#" LINES TERMINATED BY "!!!";

mysql> SYSTEM ls /var/lib/mysql-files/
a.txt  b.txt  c.txt  passwd

[root@server51 ~]# cat /var/lib/mysql-files/a.txt 
[root@server51 ~]# cat /var/lib/mysql-files/b.txt 
[root@server51 ~]# cat /var/lib/mysql-files/c.txt 
```

#### 基础字段约束

##### 空与非空

```mysql
#约束字段是否可以赋NULL值
mysql> USE execdb;

mysql> CREATE TABLE t1(
    -> n1 CHAR(10),
    -> n2 CHAR(10) NOT NULL
    -> );											#创建t1表测试非空约束

mysql> DESC t1;

mysql> INSERT INTO t1 VALUES ('zs','zhsan');		#写入成功

mysql> INSERT INTO t1 VALUES (NULL,'zhsan');		#写入成功

mysql> INSERT INTO t1 VALUES ('zs',NULL);			#写入失败
ERROR 1048 (23000): Column 'n2' cannot be null

mysql> SELECT * FROM t1;
```

##### 默认值

```mysql
#默认值约束，赋值过程中如果不指定则使用默认值
mysql> CREATE TABLE t2 ( 
    -> name CHAR(10), 
    -> age TINYINT UNSIGNED DEFAULT 18 
	-> );											#创建t2表测试默认值约束

mysql> DESC t2;

mysql> INSERT INTO t2 VALUES ('zhangsan',20);		#全字段赋值，写入成功

mysql> INSERT INTO t2(name) VALUES ('lisi');		#指定字段赋值，写入成功

mysql> SELECT * FROM t2;							#确认lisi用户年龄
```

##### 唯一约束

```mysql
#UNIQUE约束，不允许字段值重复但可以赋空值
mysql> CREATE TABLE t3( 
    -> name CHAR(20),
    -> email CHAR(20) UNIQUE
	-> )DEFAULT CHARSET=utf8;						#创建t3表测试唯一约束

mysql> DESC t3;

mysql> INSERT INTO t3 VALUES ('张扬',NULL);					#email可以赋空，写入成功

mysql> INSERT INTO t3 VALUES ('张杨','zhangyang@tedu.cn');	#写入成功

mysql> INSERT INTO t3 VALUES ('张洋','zhangyang@tedu.cn');	#email字段重复，写入失败
ERROR 1062 (23000): Duplicate entry 'zhangyang@tedu.cn' for key 'email'

mysql> INSERT INTO t3 VALUES ('张洋','zhangyang1@tedu.cn');	#写入成功
Query OK, 1 row affected (0.00 sec)

mysql> SELECT * FROM t3;
```

#### 高级字段约束

##### 主键

```mysql
#主键约束：用于保证字段值具有唯一性并且非空，一张表中只能有一个主键

#单列主键测试
mysql> USE execdb;											#使用execdb库

mysql> CREATE TABLE t4 (name CHAR(5) PRIMARY KEY);			#设置主键约束语法1

mysql> DESC t4;

mysql> CREATE TABLE t5 (name CHAR(5),PRIMARY KEY(name));	#设置主键约束语法2

mysql> DESC t5;

mysql> INSERT INTO t5 VALUES ('zhsan');						#写入成功

mysql> INSERT INTO t5 VALUES ('zhsan');						#值重复，写入失败
ERROR 1062 (23000): Duplicate entry 'zhsan' for key 'PRIMARY'

mysql> INSERT INTO t5 VALUES (NULL);						#值为空，写入失败
ERROR 1048 (23000): Column 'name' cannot be null

mysql> ALTER TABLE t5 DROP PRIMARY KEY;						#删除已有主键

mysql> DESC t5;

mysql> ALTER TABLE t5 ADD PRIMARY KEY(name);				#添加主键约束

mysql> DESC t5;

#复合主键测试(多字段组合结果不重复即可)
mysql> CREATE TABLE t6 (
    -> user CHAR(10),
    -> host CHAR(15),
    -> status ENUM('allow','deny') NOT NULL DEFAULT "deny",
    -> PRIMARY KEY(user,host)
    -> );											#创建t6测试复合主键

mysql> DESC t6;

mysql> INSERT INTO t6 VALUES
    -> ('abc','1.1.1.1','allow'),
    -> ('abc','1.1.1.2','deny'),
    -> ('haha','1.1.1.1','deny');					#写入成功

mysql> INSERT INTO t6 VALUES
    -> ('haha','1.1.1.1','allow');					#组合结果与已有数据冲突，写入失败
ERROR 1062 (23000): Duplicate entry 'haha-1.1.1.1' for key 'PRIMARY'

mysql> ALTER TABLE t6 DROP PRIMARY KEY;				#删除已有复合主键

mysql> DESC t6;

mysql> ALTER TABLE t6 ADD PRIMARY KEY(user,host);	#向已有表添加复合主键

mysql> DESC t6;

#自增长测试
mysql> CREATE TABLE t7 (
    -> id INT PRIMARY KEY AUTO_INCREMENT,
    -> name CHAR(10)
    -> );									#PRIMARY KEY与AUTO_iNCREMENT联用

mysql> DESC t7;

mysql> CREATE TABLE t8(
    -> id INT AUTO_INCREMENT,
    -> name CHAR(10),
    -> PRIMARY KEY(id)
    -> );									#PRIMARY KEY与AUTO_INCREMENT分开用

mysql> DESC t8;

mysql> INSERT INTO t8(name) VALUES ('bob');		#id从1开始

mysql> INSERT INTO t8(name) VALUES ('tom');		#id为2

mysql> SELECT * FROM t8;

mysql> INSERT INTO t8 VALUES (10,'jim');		#可以指定id赋值

mysql> INSERT INTO t8(name) VALUES ('john');	#识别id字段最大值

mysql> SELECT * FROM t8;

mysql> DELETE FROM t8;							#清空表记录，验证自增长是否重置

mysql> INSERT INTO t8(name) VALUES ('haha');	#测试写入

mysql> SELECT * FROM t8;						#自增长未重置

mysql> TRUNCATE TABLE t8;						#TRUNCATE语句快速清表

mysql> INSERT INTO t8(name) VALUES ('haha');	#测试写入记录

mysql> SELECT * FROM t8;						#id自增长重置

mysql> DELETE FROM t8;

mysql> ALTER TABLE t8 AUTO_INCREMENT=1;			#手动重置自增长

mysql> INSERT INTO t8(name) VALUES ('haha');	#测试写入记录

mysql> SELECT * FROM t8;

#主键不一定自增，但被标记自增的一定是主键
```

##### 外键

```mysql
#外键约束：保证数据的一致性，外键字段值必须在参考表中字段已有值里选择，一张表中可以有多个外键
#表存储引擎必须是innodb （在进阶课程里讲  现在仅需要知道如何指定表使用innodb存储引擎）
#外键表字段与被关联表字段类型要一致
#被参照字段必须要是索引类型的一种(通常是PRIMARY KEY)

mysql> SHOW VARIABLES LIKE "default_storage_engine";
+------------------------+--------+
| Variable_name          | Value  |
+------------------------+--------+
| default_storage_engine | InnoDB |
+------------------------+--------+
1 row in set (0.00 sec)

##参考之前的练习数据表tarena.employees和tarena.salary创建练习表

mysql> DESC tarena.employees;				#查看tarena.employees表结构

mysql> DESC tarena.salary;					#查看tarena.salary表结构

mysql> USE execdb;							#切换到练习库

mysql> CREATE TABLE emp (
    -> id INT PRIMARY KEY AUTO_INCREMENT,
    -> name CHAR(20) 
    -> );									#创建练习emp表，用于存储员工信息
Query OK, 0 rows affected (0.00 sec)

mysql> DESC emp;							#查看emp表结构
+-------+----------+------+-----+---------+----------------+
| Field | Type     | Null | Key | Default | Extra          |
+-------+----------+------+-----+---------+----------------+
| id    | int(11)  | NO   | PRI | NULL    | auto_increment |
| name  | char(20) | YES  |     | NULL    |                |
+-------+----------+------+-----+---------+----------------+
2 rows in set (0.00 sec)


mysql> CREATE TABLE salary( 
    -> id INT, 
    -> wage FLOAT,
    -> FOREIGN KEY(id) REFERENCES emp(id)
    -> ON UPDATE CASCADE
    -> ON DELETE CASCADE
	-> );									#创建练习salary表，用于存储员工某个月工资
Query OK, 0 rows affected (0.00 sec)
	
mysql> DESC salary;							#查看salary表结构
+-------+---------+------+-----+---------+-------+
| Field | Type    | Null | Key | Default | Extra |
+-------+---------+------+-----+---------+-------+
| id    | int(11) | YES  | MUL | NULL    |       |
| wage  | float   | YES  |     | NULL    |       |
+-------+---------+------+-----+---------+-------+
2 rows in set (0.00 sec)

##测试外键约束
mysql> INSERT INTO salary VALUES (1,5000);		#写入失败，emp表中id字段无值
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`execdb`.`salary`, CONSTRAINT `salary_ibfk_1` FOREIGN KEY (`id`) REFERENCES `emp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE)

mysql> INSERT INTO emp(name) VALUES ('zhangsan'),('lisi');	#添加员工记录

mysql> SELECT * FROM emp;									#查看已有员工记录

mysql> INSERT INTO salary VALUES (1,5000);					#给id是1的员工发5000，成功

mysql> INSERT INTO salary VALUES (2,4000);					#给id是2的员工发4000，成功

##验证级联更新(ON UPDATE CASCADE)
mysql> SELECT * FROM emp;						#查询emp表记录

mysql> SELECT * FROM salary;					#查询salary表记录

mysql> UPDATE emp SET id=5 WHERE name='lisi';	#更新emp表中lisi用户id为5

mysql> SELECT * FROM emp;						#确认emp表中lisi的id已更新为5

mysql> SELECT * FROM salary;					#此过程中并未操作salary表，但数据已发生变化

##验证级联删除(ON DELETE CASCADE)
mysql> SELECT * FROM emp;						#查询emp表记录

mysql> SELECT * FROM salary;					#查询salary表记录

mysql> DELETE FROM emp WHERE id=5;				#删除emp表中id为5的表记录

mysql> SELECT * FROM emp;						#确认id为5的记录被删除

mysql> SELECT * FROM salary;					#此过程中并未操作salary表，但数据已发生变化

##外键操作语法
mysql> SHOW CREATE TABLE salary;							#查看完整建表语句

mysql> ALTER TABLE salary DROP FOREIGN KEY `salary_ibfk_1`;	#删除指定外键

mysql> SHOW CREATE TABLE salary;							#查看完整建表语句

mysql> ALTER TABLE salary 
	-> ADD FOREIGN KEY(id) REFERENCES emp(id) 
	-> ON UPDATE CASCADE ON DELETE CASCADE;					#向已有表添加外键

mysql> SHOW CREATE TABLE salary;							#查看完整建表语句

##注意事项
mysql> DROP TABLE emp;										#被关联表不允许删除
ERROR 1217 (23000): Cannot delete or update a parent row: a foreign key constraint fails
mysql> 

mysql> SELECT * FROM salary;				#查看salary表记录

mysql> INSERT INTO salary VALUES (1,2000);	#给员工重复发工资

mysql> SELECT * FROM salary;				#重复工资下发成功，该现象不合理

mysql> DESC salary;							#查看salary表结构，id字段不能重复(PRIMARY KEY)

mysql> ALTER TABLE salary ADD PRIMARY KEY(id);	#给salary表id字段添加主键，失败，数据重复
ERROR 1062 (23000): Duplicate entry '1' for key 'PRIMARY'
mysql> DELETE FROM salary WHERE wage=2000;		#清理重复数据

mysql> ALTER TABLE salary ADD PRIMARY KEY(id);	#给salary表id字段添加主键，成功

mysql> DESC salary;								#查看salary表结构，id字段标签

mysql> INSERT INTO salary VALUES (1,2000);		#再次测试重复工资，下发失败
ERROR 1062 (23000): Duplicate entry '1' for key 'PRIMARY'
mysql> SHOW CREATE TABLE salary;				#查看salary建表语句
#PRIMARY KEY和FOREIGN KEY可以共存
```

