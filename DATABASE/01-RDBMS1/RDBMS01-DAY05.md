### RDBMS1-DAY05-笔记

***

#### 数据备份

##### 为什么要备份

数据是企业生存的命脉

##### 什么是备份

将数据另外保存一份

##### 备份到哪里

通常采用异地保存

##### 什么时候备份

备份的窗口期，通常是业务压力最低点

##### 如何备份

- 备份方法
  - 物理备份
  - 逻辑备份
- 备份策略
  - 完整备份
  - 增量备份
  - 差异备份
- 备份三要素
  - BW：完成备份需要的时间
  - RPO：客户可承受的最大数据丢失量
  - RTO：客户可承受的最长停机时间

##### 容灾级别

| 级别 | 说明         |
| ---- | ------------ |
| 0    | 无异地备份   |
| 1    | 有异地备份   |
| 2    | 实现热备份   |
| 3    | 在线恢复数据 |
| 4    | 定时备份数据 |
| 5    | 实时备份数据 |
| 6    | 零数据丢失   |

#### 完整备份

##### 物理备份(cp、tar、zip)

```shell
##物理备份及恢复测试：使用cp、tar、zip等命令对数据库磁盘文件进行备份

#server51操作

[root@server51 ~]# systemctl stop mysqld						#停止MySQL服务
[root@server51 ~]# mkdir /bak									#创建备份文件存储目录
[root@server51 ~]# tar -zcPf /bak/db.tar.gz /var/lib/mysql/*	#对MySQL磁盘文件打包
[root@server51 ~]# ls /bak/										#确认备份成功
db.tar.gz

[root@server51 ~]# rm -rf /var/lib/mysql/*						#删除MySQL磁盘文件模拟数据丢失
[root@server51 ~]# tar -zxPf /bak/db.tar.gz -C /				#解压备份数据，恢复数据
[root@server51 ~]# ls /var/lib/mysql/							#确认数据目录下有文件
[root@server51 ~]# chown -R mysql.mysql /var/lib/mysql/			#修改文件归属！！！

[root@server51 ~]# systemctl start mysqld						#启动服务测试数据

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW DATABASES;"
```

##### 逻辑备份(mysqldump、mysql)

```mysql
#mysqldump命令用于备份数据
#mysql命令用于恢复数据

##逻辑备份及恢复测试

#备份指定库下的多个表
[root@server51 ~]# mysqldump -hlocalhost -uroot -p'123qqq...A' tarena departments employees salary > /bak/des.sql

#备份多个指定库下的所有表
[root@server51 ~]# mysqldump -hlocalhost -uroot -p'123qqq...A' -B tarena execdb > /bak/te.sql

#备份所有库下的表
[root@server51 ~]# mysqldump -hlocalhost -uroot -p'123qqq...A' -A > /bak/all.sql

[root@server51 ~]# ls /bak/*.sql			#确认数据备份成功
/bak/all.sql  /bak/des.sql  /bak/te.sql

##测试恢复指定库下的表

#删除tarena库下的表模拟数据丢失，注意顺序不能改
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "DROP TABLE tarena.salary; DROP TABLE tarena.employees; DROP TABLE tarena.departments;"

#确认departments表、employees表和salary表被删除
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW TABLES FROM tarena;"

#利用des.sql恢复数据
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' tarena < /bak/des.sql 

#确认3张表恢复成功
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW TABLES FROM tarena;"

##测试恢复多个库

#删除tarena库和execdb库模拟数据丢失
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "DROP DATABASE tarena; DROP DATABASE execdb;"

#确认tarena库和execdb库被删除
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW DATABASES;"

#利用te.sql恢复两个库
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' < /bak/te.sql 

#确认两个库恢复成功
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW DATABASES;"

##测试恢复所有库(注意含有server52操作)

#发送all.sql到server52
[root@server51 ~]# scp /bak/all.sql 192.168.88.52:/root	

#确认server52上只有初始库
[root@server52 ~]# mysql -hlocalhost -uroot -p'123456' -e "SHOW DATABASES;"

#确认all.sql存在
[root@server52 ~]# ls /root/all.sql 

#恢复所有库
[root@server52 ~]# mysql -hlocalhost -uroot -p'123456' < all.sql 

#确认库恢复情况
[root@server52 ~]# mysql -hlocalhost -uroot -p'123456' -e "SHOW DATABASES;"

#小问题处理(此时依旧使用旧密码登录，由于服务没重启导致还没加载新的mysql.user文件)
[root@server52 ~]# systemctl restart mysqld
[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW DATABASES;"
```

##### 逻辑备份应用案例

```shell
##编写计划任务执行脚本实现每周周一晚上23点备份服务的所有数据，用系统日期做备份文件名

[root@server51 ~]# vim allbak.sh			#编写备份数据库所有库脚本
[root@server51 ~]# cat allbak.sh 
#!/bin/bash
mysqldump -hlocalhost -uroot -p'123qqq...A' -A > /bak/`date +%F`_all.sql
[root@server51 ~]# chmod +x ./allbak.sh 	#给脚本添加x权限

[root@server51 ~]# crontab -e 				#编写root用户计划任务
no crontab for root - using an empty one
crontab: installing new crontab
[root@server51 ~]# crontab -l				#计划任务内容为每周一晚上23点执行allbak.sh脚本
0 23 * * 1 /bin/bash /root/allbak.sh

[root@server51 ~]# systemctl enable crond.service 	#确保crond服务开机自启动
[root@server51 ~]# systemctl start crond.service 	#确保crond服务启动
[root@server51 ~]# 
```

#### 增量备份(Percona-XtraBackup)

```shell
##利用Percona-XtraBackup软件实现数据库完整备份与增量备份(server51和server52操作)

##更新自定义Yum源mysqlsofts
[root@pubserver ~]# cd /var/ftp/mysqlsofts/		#切换到自定义mysqlsofts源上传软件(2个)
[root@pubserver mysqlsofts]# ls libev-4.15-1.el6.rf.x86_64.rpm percona-xtrabackup-24-2.4.7-1.el7.x86_64.rpm 
libev-4.15-1.el6.rf.x86_64.rpm  percona-xtrabackup-24-2.4.7-1.el7.x86_64.rpm
[root@pubserver mysqlsofts]# createrepo --update ./			#更新自定义yum源repodata信息
[root@pubserver mysqlsofts]# yum clean all; yum repolist	#确认yum源是否可用及是否更新
...
repolist: 10,085			#相比之前多了2个软件包
[root@pubserver mysqlsofts]# cd
[root@pubserver ~]# 

#server51安装Percona-XtraBackup软件
[root@server51 ~]# yum clean all; yum repolist					#确保yum源可用
...
repolist: 10,085			#相比之前多了2个软件包
[root@server51 ~]# yum -y install percona-xtrabackup-24.x86_64 	#安装Percona-XtraBackup

#server52安装Percona-XtraBackup软件
[root@server52 ~]# yum clean all; yum repolist
repolist: 10,085
[root@server52 ~]# yum -y install percona-xtrabackup-24.x86_64 

##server51上做数据完整备份和增量备份
#策略：0点做完整备份，6点、12点、18点做增量备份

#创建测试数据
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'	#登录server51的MySQL服务

mysql> CREATE TABLE execdb.role(name CHAR(20));				#创建测试表role

mysql> INSERT INTO execdb.role VALUES ('tom'),('bob');		#写入两条数据

mysql> SELECT * FROM execdb.role;							#确认数据写入

mysql> exit
Bye
[root@server51 ~]# 

#模拟0点做完整备份
[root@server51 ~]# mkdir /bak/fi				#创建存储Percona备份数据的目录
[root@server51 ~]# innobackupex -u root -p '123qqq...A' --no-timestamp /bak/fi/full	#使用Percona-XtraBackup做完整备份(所有库)
...
230513 03:36:35 completed OK!
[root@server51 ~]# ls /bak/fi/full/				#确保备份数据目录下有数据

#模拟6点做增量备份
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "INSERT INTO execdb.role VALUES ('zhangsan');"							   	#写入新数据zhangsan

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"										  #确认数据新增成功

[root@server51 ~]# innobackupex -u root -p '123qqq...A' --no-timestamp --incremental --incremental-basedir=/bak/fi/full /bak/fi/incr1		#做第1次增量备，参考完整备
...
230513 03:42:09 completed OK!
[root@server51 ~]# ls /bak/fi/incr1/				 

#模拟12点做增量备份
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "INSERT INTO execdb.role VALUES ('lisi');"									#写入新数据lisi

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"										  #确认数据新增成功

[root@server51 ~]# innobackupex -u root -p '123qqq...A' --no-timestamp --incremental --incremental-basedir=/bak/fi/incr1 /bak/fi/incr2		#做第2次增量备，参考第1次增量备
...
230513 03:43:39 completed OK!
[root@server51 ~]# ls /bak/fi/incr2/

#模拟18点做增量备份
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "INSERT INTO execdb.role VALUES ('wangwu');"									#写入新数据wangwu

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"										  #确认数据新增成功

[root@server51 ~]# innobackupex -u root -p '123qqq...A' --no-timestamp --incremental --incremental-basedir=/bak/fi/incr2 /bak/fi/incr3		#做第3次增量备，参考第2次增量备
...
230513 03:44:24 completed OK!
[root@server51 ~]# ls /bak/fi/incr3/

##server51打包完整备份和增量备份数据发送到server52
[root@server51 ~]# tar -zcvf fi.tar.gz /bak/fi/			#打包备份数据
[root@server51 ~]# scp fi.tar.gz 192.168.88.52:/root	#发送备份数据


##server52上做数据完整备份和增量备份恢复
#策略：将所有增量备合并到完整备份统一恢复
#过程：准备恢复数据->合并准备数据->停服务->清理数据库文件->恢复数据->修改文件归属->启服务->测试

#准备恢复数据
[root@server52 ~]# ls fi.tar.gz 			
fi.tar.gz
[root@server52 ~]# tar -xf fi.tar.gz 		#解压备份数据包
[root@server52 ~]# ls /root/bak/fi/			#确认有1完整备+3增量备共4份数据
full  incr1  incr2  incr3
[root@server52 ~]# 

[root@server52 ~]# innobackupex --apply-log --redo-only /root/bak/fi/full/	#准备恢复数据
...
230513 03:53:49 completed OK!
[root@server52 ~]# 

#合并增量备份数据(所有增量备依次合并到完整备)
[root@server52 ~]# innobackupex --apply-log --redo-only --incremental-dir /root/bak/fi/incr1/ /root/bak/fi/full/		#合并incr1到full
...
230513 03:55:21 completed OK!
[root@server52 ~]# innobackupex --apply-log --redo-only --incremental-dir /root/bak/fi/incr2/ /root/bak/fi/full/		#合并incr2到full
...
[root@server52 ~]# innobackupex --apply-log --redo-only --incremental-dir /root/bak/fi/incr3/ /root/bak/fi/full/		#合并incr3到full
...
230513 03:55:55 completed OK!
[root@server52 ~]# 

#恢复数据
[root@server52 ~]# systemctl stop mysqld						#停止MySQL服务
[root@server52 ~]# ps aux | grep mysqld							#确保MySQL主进程退出
[root@server52 ~]# rm -rf /var/lib/mysql/*						#清空MySQL数据目录下文件
[root@server52 ~]# innobackupex --copy-back /root/bak/fi/full/ 	#恢复数据
...
230513 03:57:30 completed OK!
[root@server52 ~]# chown -R mysql.mysql /var/lib/mysql/			#修改数据文件归属！！！

#启动服务
[root@server52 ~]# systemctl start mysqld						#启动MySQL服务
[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"													 #测试数据恢复情况
+----------+
| name     |
+----------+
| tom      |
| bob      |
| zhangsan |
| lisi     |
| wangwu   |
+----------+
[root@server52 ~]# 
```

#### 实时备份(Binlog)

##### 启用binlog日志

```shell
##binlog是MySQL日志类型的一种，记录MySQL所有的写操作

##server51操作
[root@server51 ~]# vim /etc/my.cnf						#编辑MySQL主配置文件启用binlog
[root@server51 ~]# sed -rn '4,6p' /etc/my.cnf
[mysqld]		
log_bin			#启用binlog
server_id=51	#设置server_id值
[root@server51 ~]# systemctl restart mysqld				#重启服务使配置生效
[root@server51 ~]# ls /var/lib/mysql/server51-bin.*		#查看binlog日志相关文件
/var/lib/mysql/server51-bin.index		#binlog日志索引文件
/var/lib/mysql/server51-bin.000001 		#binlog日志文件
```

##### 测试binlog日志

```mysql
#测试binlog日志
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'	#登录MySQL服务

mysql> SHOW BINARY LOGS;							#查看所有binlog日志

mysql> SHOW MASTER STATUS;							#查看当前正在使用binlog日志信息

mysql> SELECT * FROM execdb.role;					#执行读操作
		
mysql> SHOW MASTER STATUS;							#binlog日志偏移量不改变(不记录读操作)

mysql> INSERT INTO execdb.role VALUES ('zhaoliu');	#执行写操作

mysql> SHOW MASTER STATUS;							#binlog日志偏移量改变(记录写操作)
```

##### 操作binlog日志

```mysql
##操作binlog日志

##生成新的binlog日志
#生成新的binlog日志(方法一：FLUSH LOGS语句)
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'	#登录MySQL服务
mysql> SHOW BINARY LOGS;			#查看所有binlog日志(共1个)

mysql> FLUSH LOGS;					#刷新binlog日志

mysql> FLUSH LOGS;					#刷新binlog日志

mysql> FLUSH LOGS;					#刷新binlog日志

mysql> SHOW BINARY LOGS;			#刷新binlog日志(共4个)
mysql> exit
Bye
[root@server51 ~]# ls /var/lib/mysql/server51-bin.*			#查看binlog日志磁盘文件

#生成新的binlog日志(方法二：重启MySQL服务)
[root@server51 ~]# systemctl restart mysqld					#重启MySQL服务

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW BINARY LOGS;"	#查看已有binlog日志(共5个)

[root@server51 ~]# ls /var/lib/mysql/server51-bin.*			#查看binlog日志磁盘文件

#生成新的binlog日志(方法二：mysqldump -F)
[root@server51 ~]# mysqldump --help							#查看mysqldump命令帮助信息

[root@server51 ~]# mysqldump -hlocalhost -uroot -p'123qqq...A' -A -F --lock-all-tables > /bak/ab.sql											   #备份数据同时刷新binlog日志
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW BINARY LOGS;"	#查看已有binlog日志(共6个)

[root@server51 ~]# ls /var/lib/mysql/server51-bin.*			#查看binlog日志磁盘文件

##清理binlog日志
#清理无效的binlog日志(方法一：自动清理)
[root@server51 ~]# vim /etc/my.cnf				#编辑MySQL主配置文件
[root@server51 ~]# sed -rn '4,7p' /etc/my.cnf
[mysqld]
log_bin
server_id=51
expire_logs_days=7		#不活跃的binlog日志默认保留天数
[root@server51 ~]# 								#需重启服务生效，此处不演示了

#清理无效的binlog日志(方法二：手动清理)
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'	#登录MySQL服务

mysql> SHOW BINARY LOGS;							#查看已有binlog日志(6个)

mysql> PURGE MASTER LOGS TO "server51-bin.000005";	#清理到指定binlog日志

mysql> SHOW BINARY LOGS;							#查看剩余binlog日志(2个)

#重置binlog日志
mysql> RESET MASTER;								#重置binlog日志

mysql> SHOW BINARY LOGS;							#查看已有binlog日志(1个且编号为1)
```

##### 自定义binlog日志

```shell
#自定义binlog日志存储位置及文件名
[root@server51 ~]# vim /etc/my.cnf					#编辑MySQL主配置文件
[root@server51 ~]# sed -rn '4,7p' /etc/my.cnf
[mysqld]
log_bin=/mylog/db1		#指定binlog日志存储文件和文件名
server_id=51
expire_logs_days=7
[root@server51 ~]# mkdir /mylog/					#创建自定义binlog存储目录
[root@server51 ~]# chown mysql.mysql /mylog/		#将目录归属改为MySQL用户！！
[root@server51 ~]# getenforce 						#确保SELinux关闭
Disabled

[root@server51 ~]# systemctl restart mysqld			#重启服务
[root@server51 ~]# ls /mylog/						#查看自定义binlog日志存储目录
db1.000001  db1.index
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW BINARY LOGS;"	#查看已有binlog日志信息，旧的binlog日志已经作废
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+-----------+
| Log_name   | File_size |
+------------+-----------+
| db1.000001 |       154 |
+------------+-----------+
[root@server51 ~]# 
```

##### 查看binlog日志内容

```shell
##之前操作中已知binlog日志内容会变化

#查看binlog日志
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'		#登录MySQL服务

mysql> SHOW MASTER STATUS;						#查看正在使用的binlog日志信息

mysql> INSERT INTO execdb.role VALUES ('lily');	#执行写操作

mysql> SHOW MASTER STATUS;						#查看正在使用的binlog日志信息

mysql> SHOW BINLOG EVENTS IN "db1.000001";		#查询指定binlog日志中的事件

mysql> exit
Bye
[root@server51 ~]# mysqlbinlog /mylog/db1.000001 
...
#230513  4:36:21 server id 51  end_log_pos 379 CRC32 0xd9e1d239         Write_rows: table id 109 flags: STMT_END_F

BINLOG '
RaNeZBMzAAAAMwAAAFIBAAAAAG0AAAAAAAEABmV4ZWNkYgAEcm9sZQAB/gL+FAGbuuzC
RaNeZB4zAAAAKQAAAHsBAAAAAG0AAAAAAAEAAgAB//4EbGlseTnS4dk=
'/*!*/;
# at 379
#230513  4:36:21 server id 51  end_log_pos 410 CRC32 0x758e0521         Xid = 8
COMMIT/*!*/;
SET @@SESSION.GTID_NEXT= 'AUTOMATIC' /* added by mysqlbinlog */ /*!*/;
DELIMITER ;
# End of log file
/*!50003 SET COMPLETION_TYPE=@OLD_COMPLETION_TYPE*/;
/*!50530 SET @@SESSION.PSEUDO_SLAVE_MODE=0*/;
[root@server51 ~]# 
```

##### 使用binlog日志恢复数据

```shell
##由于binlog日志记录MySQL所有写操作的特性，使得binlog日志配合mysqldump可实现完整备份和增量备份功能

#mysqldump做完整备份，binlog做增量备份(server51操作)
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"						 	 #查看execdb.role已有记录

[root@server51 ~]# ls /mylog/			#查看binlog日志情况
db1.000001  db1.index

[root@server51 ~]# mysqldump -hlocalhost -uroot -p'123qqq...A' -F --lock-all-tables execdb role > /bak/role.sql				#对execdb.role做备份(完整备)

[root@server51 ~]# cat /bak/role.sql 	#确保数据备份成功

[root@server51 ~]# ls /mylog/			#查看binlog日志情况
db1.000001  db1.000002  db1.index

[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "INSERT INTO execdb.role VALUES ('lucy');"					   #写入新数据，该操作记录在db1.000002中(增量备)

[root@server51 ~]# scp /bak/role.sql 192.168.88.52:/root		#发送SQL文件
[root@server51 ~]# scp /mylog/db1.000002 192.168.88.52:/root	#发送binlog日志文件

#利用SQL文件和binlog日志恢复数据(server52操作)
[root@server52 ~]# ls role.sql db1.000002 		#查看还原数据需要的文件
db1.000002  role.sql
[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "DROP TABLE execdb.role;"								#删除execdb.role表

[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A' execdb < role.sql 									 #还原sql文件内的数据(完整恢复)

[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"				  #查看数据还原情况

[root@server52 ~]# mysqlbinlog db1.000002 | mysql -hlocalhost -uroot -p'123qqq...A'								 #还原binlog日志内的数据(增量恢复)

[root@server52 ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SELECT * FROM execdb.role;"				  #查看数据还原情况
+----------+
| name     |
+----------+
| tom      |
| bob      |
| zhangsan |
| lisi     |
| wangwu   |
| zhaoliu  |
| lily     |
| lucy     |
+----------+
[root@server52 ~]# 
```



##### 