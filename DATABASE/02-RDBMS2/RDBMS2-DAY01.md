### RDBMS2-DAY01-笔记

***

#### MySQL用户权限管理

##### 什么是用户权限管理

MySQL是一个支持多用户操作的服务

如果管理这些用户能做什么事是重中之重

##### MySQL用户管理

```mysql
##用户相关操作

#用户与登录地址绑定：用户@登录地址
/*
	user@%				：所有地址
	user@192.168.88.%	：192.168.88.0网段
	user@192.168.88.1	：192.168.88.1主机
	user@localhost		：本机
*/
	
	
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'		#登录MySQL服务

mysql> SELECT user,host FROM mysql.user;		#查看已有用户
+-----------+-----------+
| user      | host      |
+-----------+-----------+
| mysql.sys | localhost |
| root      | localhost |
+-----------+-----------+
2 rows in set (0.00 sec)

mysql> CREATE USER root@'192.168.88.52' IDENTIFIED BY '123qqq...A';	#创建新用户
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT user,host FROM mysql.user;		#查看已有用户
+-----------+---------------+
| user      | host          |
+-----------+---------------+
| root      | 192.168.88.52 |
| mysql.sys | localhost     |
| root      | localhost     |
+-----------+---------------+
3 rows in set (0.00 sec)

mysql> ALTER USER root@'192.168.88.52' IDENTIFIED BY 'nsd2022...A';		#修改用户密码
Query OK, 0 rows affected (0.00 sec)

mysql> SET PASSWORD FOR root@'192.168.88.52'=PASSWORD('123qqq...A');	#修改用户密码
Query OK, 0 rows affected, 1 warning (0.00 sec)

mysql> RENAME USER root@'192.168.88.52' TO admin@'192.168.88.52';		#修改用户名
Query OK, 0 rows affected (0.01 sec)

mysql> SELECT user,host FROM mysql.user;		#查看已有用户
+-----------+---------------+
| user      | host          |
+-----------+---------------+
| admin     | 192.168.88.52 |
| mysql.sys | localhost     |
| root      | localhost     |
+-----------+---------------+
3 rows in set (0.00 sec)

mysql> DROP USER admin@'localhost';				#删除用户
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT user,host FROM mysql.user;		#查看已有用户
+-----------+-----------+
| user      | host      |
+-----------+-----------+
| mysql.sys | localhost |
| root      | localhost |
+-----------+-----------+
2 rows in set (0.00 sec)
```

##### MySQL权限管理

```mysql
##用户权限相关操作

mysql> CREATE USER root@'192.168.88.52' IDENTIFIED BY '123qqq...A';	#创建新用户

mysql> SHOW GRANTS FOR root@'192.168.88.52';				#查看指定用户权限(Usage可登录)

mysql> SHOW GRANTS;											#查看当前用户权限

mysql> GRANT SELECT ON *.* TO root@'192.168.88.52';			#用户授权

mysql> SHOW GRANTS FOR root@'192.168.88.52';				#查看指定用户权限

mysql> GRANT UPDATE,DELETE ON *.* TO root@'192.168.88.52';	#用户授权(叠加操作)

mysql> SHOW GRANTS FOR root@'192.168.88.52';				#查看指定用户权限

mysql> GRANT ALL ON *.* TO root@'192.168.88.52';			#ALL为MySQL的所有权限

mysql> SHOW GRANTS FOR root@'192.168.88.52';				#查看指定用户权限

mysql> REVOKE INSERT,DELETE,DROP ON *.* FROM root@'192.168.88.52';	#削减指定权限

mysql> SHOW GRANTS FOR root@'192.168.88.52';				#查看指定用户权限

mysql> REVOKE ALL ON *.* FROM root@'192.168.88.52';			#削减用户拥有的所有权限

mysql> SHOW GRANTS FOR root@'192.168.88.52';				#ALL不包含Usage

#目前版本中GRANT关键字支持创建不存在的用户，高版本中不支持
mysql> GRANT SELECT ON *.* TO admin@'localhost' IDENTIFIED BY '123qqq...A';	
mysql> SELECT user,host FROM mysql.user;
```

##### MySQL权限分布

```mysql
##用户创建及授权是持久化操作，即用户信息及用户权限保存在表中

/*
	相关表：
			mysql.user			：用户信息及全局权限
			mysql.db			：指定库下所有表权限
			mysql.tables_priv	：指定库下指定表权限
			mysql.columns_priv	：指定库下指定表指定字段权限
*/

#查看相关表结构
mysql> DESC mysql.user;
mysql> DESC mysql.db;
mysql> DESC mysql.tables_priv;
mysql> DESC mysql.columns_priv;

##测试权限分布
#测试用户信息
mysql> CREATE USER admin@'localhost' IDENTIFIED BY '123qqq...A';	#创建新用户

mysql> SELECT * FROM mysql.user WHERE user='admin'\G				#查看用户信息

#测试指定库下指定表指定字段权限
mysql> GRANT UPDATE(shell) ON tarena.user TO admin@'localhost';		#用户授权

mysql> SHOW GRANTS FOR admin@'localhost';							#查看用户权限

mysql> SELECT * FROM mysql.columns_priv;							#查看权限分布

mysql> SELECT * FROM mysql.tables_priv;								#查看权限分布

#测试指定库下指定表权限
mysql> GRANT SELECT ON tarena.user TO admin@'localhost';			#用户授权

mysql> SHOW GRANTS FOR admin@'localhost';							#查看用户权限

mysql> SELECT * FROM mysql.tables_priv;								#查看权限分布

#测试指定库下所有表权限
mysql> GRANT DELETE ON tarena.* TO admin@'localhost';				#用户授权

mysql> SHOW GRANTS FOR admin@'localhost';							#查看用户权限

mysql> SELECT * FROM mysql.db\G										#查看权限分布

#测试所有库所有表(全局)权限
mysql> GRANT INSERT ON *.* TO admin@'localhost';					#用户授权

mysql> SHOW GRANTS FOR admin@'localhost';							#查看用户权限

mysql> SELECT * FROM mysql.user WHERE user='admin'\G				#查看权限分布
```

#### MySQL索引

##### 索引是什么

索引是为了加速查询的一种数据结构

索引分类：

- 功能
  - 普通索引
  - 唯一索引
  - 全文索引
- 作用字段
  - 单列索引
  - 多列索引

##### 索引操作

```mysql
##索引操作

#创建索引
mysql> CREATE TABLE execdb.t11(name CHAR(20),uid INT, shell CHAR(20),INDEX(name),INDEX(uid),INDEX(shell));	   #建表时创建索引

mysql> DESC execdb.t11;								#查看表结构MUL

#已有表添加索引
mysql> CREATE TABLE execdb.t12(name CHAR(20),uid INT, shell CHAR(20));	#建表

mysql> DESC execdb.t12;							#查看表结构

mysql> CREATE INDEX name ON execdb.t12(name);	#添加索引

mysql> DESC execdb.t12;							#查看表结构

#查看具体索引信息
mysql> SHOW INDEX FROM execdb.t12\G				#查看表中所有索引
*************************** 1. row ***************************
        Table: t12		#表名
   Non_unique: 1		#是否为唯一索引，是0否1
     Key_name: name		#索引名称
 Seq_in_index: 1		#该列在索引中的位置，因为有组合索引
  Column_name: name		#字段名
    Collation: A		#列以何种顺序存储在索引中，A为升序，NULL表示无分类
  Cardinality: 0		#索引中唯一数目的估计值
     Sub_part: NULL		#列中被编入索引字符的数量，整列编入显示NULL
       Packed: NULL		#关键字如何被压缩，NULL表示没有被压缩
         Null: YES		#索引列中是否包含NULL
   Index_type: BTREE	#索引类别（BTREE、FULLTEXT、HASH、RTREE）
      Comment: 			#显示评注
Index_comment: 			#索引单独评注

#删除已有索引
mysql> DROP INDEX name ON execdb.t12;	#删除指定索引

mysql> DESC execdb.t12;					#查看表结构
```

##### 索引案例

```shell
#编写shell脚本生成1000000条数据
[root@server51 ~]# vim gendata.sh 
[root@server51 ~]# cat gendata.sh 
#!/bin/bash
#向execdb.t12表写入1000000条数据

shells=("/bin/bash" "/sbin/nologin" "/bin/false" "/sbin/shutdown")
for i in {1..1000000}
do
    name="name$i"
    uid=$i
    num=$[RANDOM%4]
    shell="${shells[$num]}"
    echo ${name} ${uid} ${shell}
    mysql -hlocalhost -uroot -p'123qqq...A' -e "INSERT INTO execdb.t12 VALUES(\"${name}\",${uid},\"${shell}\")" &> /dev/null
    if [ $[i%10000] -eq 0 ];then
        echo "已经写入${i}条数据"
    fi
done
[root@server51 ~]# bash gendata.sh 
```

##### 索引引用

```mysql

#explain语句各个字段解释如下：
id： 表示当前select语句的编号，该值可能为空，如果行联合了其他行的结果；在这种情况下table列显示的是，引用的行的并集。
select_type： 这个值有很多，暂时可以先记以下几个：
　　SIMPLE: 简单查询，不包含连接查询和子查询。
PRIMARY: 最外层查询，主键查询
UNION：连接查询的第二个或后面的查询语句。　其余参数可以查看https://dev.mysql.com/doc/refman/5.7/en/explain-output.html
  table： 查询的表名

partitions：显示查询使用的分区，若为NULL则未使用分区。
type：表示表的连接类型，有如下取值：
const   ：表示表中有多条记录，但只从表中查询一条记录;
eq_ref ：表示多表连接时，后面的表使用了UNIQUE或者PRIMARY KEY;
ref       ：表示多表查询时，后面的表使用了普通索引;
unique_ subquery：表示子查询中使用了UNIQUE或者PRIMARY KEY;
index_ subquery：表示子查询中使用了普通索引;
range  ：表示查询语句中给出了查询范围;
index   ：表示对表中的索引进行了完整的扫描;
all        ：表示此次查询进行了全表扫描;（一般来说全表扫描需要优化，表的记录很少除外）
possible_keys：表示查询中可能使用的索引；如果备选的数量大于3那说明已经太多了，因为太多会导致选择索引而损耗性能， 所以建表时字段最好精简，同时也要建立联合索引，避免无效的单列索引；
key： 查询实际使用的索引（不太准确，可以查阅官方文档）。
key_len：索引的长度
ref： REF列显示哪些列或常量与键列中所命名的索引进行比较，以从表中选择行。
rows： 查询扫描的行数。
filtered：表示按条件过滤表行的百分比，最大为100表示100%。
Extra： 表示查询额外的附加信息说明。
```



#### MySQL视图

##### 视图是什么



##### 视图操作



#### MySQL存储过程

##### 存储过程是什么



##### 存储过程操作