### RDBMS-DAY02-笔记

***

#### 练习数据解析

##### 确认表导入成功

```shell
##server51操作

#登录数据库
[root@server51 ~]# mysql -hlocalhost -uroot -p'123qqq...A'

#确认数据已经导入
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| tarena             |
+--------------------+
5 rows in set (0.00 sec)

mysql> SHOW TABLES FROM tarena;
+------------------+
| Tables_in_tarena |
+------------------+
| departments      |
| employees        |
| salary           |
| user             |
+------------------+
4 rows in set (0.00 sec)

mysql> USE tarena;
```

##### 练习用表解析

```mysql
#查看表结构
mysql> DESC tarena.departments;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| dept_id   | int(4)      | NO   | PRI | NULL    | auto_increment |
| dept_name | varchar(10) | YES  |     | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
2 rows in set (0.00 sec)

mysql> DESC tarena.employees;
+--------------+-------------+------+-----+---------+----------------+
| Field        | Type        | Null | Key | Default | Extra          |
+--------------+-------------+------+-----+---------+----------------+
| employee_id  | int(6)      | NO   | PRI | NULL    | auto_increment |
| name         | varchar(10) | YES  |     | NULL    |                |
| hire_date    | date        | YES  |     | NULL    |                |
| birth_date   | date        | YES  |     | NULL    |                |
| email        | varchar(25) | YES  |     | NULL    |                |
| phone_number | char(11)    | YES  |     | NULL    |                |
| dept_id      | int(4)      | YES  | MUL | NULL    |                |
+--------------+-------------+------+-----+---------+----------------+
7 rows in set (0.00 sec)

mysql> DESC salary;
+-------------+---------+------+-----+---------+----------------+
| Field       | Type    | Null | Key | Default | Extra          |
+-------------+---------+------+-----+---------+----------------+
| id          | int(11) | NO   | PRI | NULL    | auto_increment |
| date        | date    | YES  |     | NULL    |                |
| employee_id | int(6)  | YES  | MUL | NULL    |                |
| basic       | int(6)  | YES  |     | NULL    |                |
| bonus       | int(6)  | YES  |     | NULL    |                |
+-------------+---------+------+-----+---------+----------------+
5 rows in set (0.00 sec)

#确认表内有数据
mysql> SELECT * FROM tarena.departments;	#查看部门表所有数据

mysql> SELECT * FROM tarena.employees;		#查看员工表所有数据

mysql> SELECT * FROM tarena.salary;			#查看工资表所有数据
```

##### 表格结构设计

- departments部门表：共8个部门

| 字段      | 类型        | 说明     |
| --------- | ----------- | -------- |
| dept_id   | INT(4)      | 部门编号 |
| dept_name | VARCHAR(10) | 部门名称 |

- employees员工表：共133位员工隶属于不同部门

| 字段         | 类型        | 说明         |
| ------------ | ----------- | ------------ |
| employee_id  | INT(6)      | 员工工号     |
| name         | VARCHAR()   | 姓名         |
| hire_data    | DATE        | 入职日期     |
| birth_date   | DATE        | 生日         |
| email        | VARCHAR(25) | 邮箱         |
| phone_number | CHAR(11)    | 电话号码     |
| dept_id      | INT(4)      | 隶属部门编号 |

- salary工资表

| 字段        | 类型    | 说明     |
| ----------- | ------- | -------- |
| id          | INT(11) | 行号     |
| date        | DATE    | 发信日期 |
| employee_id | INT(6)  | 员工工号 |
| basic       | INT(6)  | 基础工资 |
| bonus       | INT(6)  | 奖金     |

![1683773511614](RDBMS01-DAY02.assets/1683773511614.png)

#### 查询语句进阶

##### 什么是MySQL函数

MySQL服务内部为实现某个功能而定义好的命令

MySQL函数格式：

- 函数()

MySQL函数用法：

- SELECT 函数();
  - SELECT NOW();
- SELECT 函数(函数());
  - SELECT YEAR(NOW());
- SELECT 函数(字段) FROM 库名.表名
  - SELECT COUNT(*) FROM tarena.user;

##### 常用功能函数

- 系统信息函数

```mysql
#系统信息函数练习

mysql> SELECT VERSION();		#显示当前数据库版本
+-----------+
| VERSION() |
+-----------+
| 5.7.17    |
+-----------+
1 row in set (0.00 sec)

mysql> SELECT DATABASE();		#显示当前正在操作的库
+------------+
| DATABASE() |
+------------+
| tarena     |
+------------+
1 row in set (0.00 sec)

mysql> SELECT USER();			#显示当前登录数据库的用户
+----------------+
| USER()         |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)
```

- 字符串函数：用于字符串处理

```mysql
#补充内容：字符与字节
ASCII 码中，一个英文字母（不分大小写）为一个字节，一个中文汉字为两个字节。
UTF-8 编码中，一个英文字为一个字节，一个中文为三个字节。
Unicode 编码中，一个英文为一个字节，一个中文为两个字节。
符号：英文标点为一个字节，中文标点为两个字节。例如：英文句号 . 占1个字节的大小，中文句号 。占2个字节的大小。
UTF-16 编码中，一个英文字母字符或一个汉字字符存储都需要 2 个字节（Unicode 扩展区的一些汉字存储需要 4 个字节）。
UTF-32 编码中，世界上任何字符的存储都需要 4 个字节。

#样例数据
mysql> SELECT name FROM tarena.employees WHERE employee_id=1;

#查询字符串长度，单位字节(bytes)
mysql> SELECT name,LENGTH(name) FROM tarena.employees WHERE employee_id=1;

#查询字符串长度，单位字符(Character)
mysql> SELECT name,CHAR_LENGTH(name) FROM tarena.employees WHERE employee_id=1;

#查询值改为大写字母
mysql> SELECT UPPER('abc');
mysql> SELECT UCASE('abc');

#查询值改为小写字母
mysql> SELECT LOWER('ABC');
mysql> SELECT LCASE('ABC');

#字符串截取:SUBSTR('string',start,end)
mysql> SELECT SUBSTR('13912345678',1,3);	#截取电话号码前3位

mysql> SELECT SUBSTR('13912345678',8,11);	#截取电话号码后4位

#字符串截取与拼接实现电话号码脱敏
mysql> SELECT CONCAT(SUBSTR('13912345678',1,3),'****',SUBSTR('13912345678',8,11));

#字符串指定位置替换:INSERT('string1',start,length,'string2')
mysql> SELECT INSERT('13912345678',4,4,'****');

#字符串匹配替换:REPLACE('string1','old','new')
mysql> SELECT REPLACE('abac','a','x');

#去空格
mysql> SELECT LTRIM('  abc  ');		#去除左边空格

mysql> SELECT RTRIM('  abc  ');		#去除右边空格

mysql> SELECT TRIM('  abc  ');		#去除两边空格
```

- 数学函数

```mysql
#数学函数练习

#ABS(x)：返回x的绝对值
mysql> SELECT ABS(3);
mysql> SELECT ABS(-3);

#PI()：返回圆周率π，默认显示6位小数
mysql> SELECT PI();

#MOD(x,y)：返回x被y除后的余数 
mysql> SELECT MOD(10,3);
mysql> SELECT id,name FROM tarena.user WHERE MOD(id,2)=0;	#查询user表中id是偶数的表记录

#CEIL(x)、CEILING(x)：返回不小于x的最小整数 (x 是小数)
mysql> SELECT CEIL(1.23);
mysql> SELECT CEILING(1.23);

#FLOOR(x)：返回不大于x的最大整数 (x 是小数)
mysql> SELECT FLOOR(1.23);

#ROUND(x):返回最接近于x的整数，即对x进行四舍五入 (x 是小数)
mysql> SELECT ROUND(1.23);	#返回1
mysql> SELECT ROUND(1.53);	#返回2

#RAND():返回0-1中间的随机数
mysql> SELECT RAND();

#FORMAT(x,y):格式化输出，用于保留小数位
mysql> SELECT FORMAT(PI(),8);		#对π保留8位小数
mysql> SELECT FORMAT(RAND()*10,0);	#生成0-10的随机数
```

- 聚集函数：用于统计，操作查询的结果

```mysql
#聚集函数练习

#avg(字段)：计算平均值
mysql> SELECT AVG(uid) FROM tarena.user;	#user表中uid的平均值

#sum(字段)：求和
mysql> SELECT SUM(uid) FROM tarena.user;	#user表中uid的和

#min(字段)：获取最小值	
mysql> SELECT MIN(uid) FROM tarena.user;	#user表中uid最小的值

#max(字段)：获取最大值
mysql> SELECT MAX(uid) FROM tarena.user;	#user表中uid最大的值

#count(字段)：统计表头值个数
mysql> SELECT COUNT(name) FROM tarena.user;	#user表中用户数
mysql> SELECT COUNT(name) FROM tarena.user WHERE shell="/bin/bash";	#user表中使用/bin/bash解释器的用户数(带条件统计)
```

- 日期时间函数

```mysql
#日期时间函数练习

#可直接调用
mysql> SELECT NOW();			#获取当前日期时间
mysql> SELECT CURDATE();		#获取当前日期
mysql> SELECT CURTIME();		#获取当前时间

#需传参获取
mysql> SELECT YEAR(NOW());		#获取当前年份
mysql> SELECT MONTH(NOW());		#获取当前月份
mysql> SELECT DAY(NOW());		#获取当前日
mysql> SELECT HOUR(NOW());		#获取当前小时
mysql> SELECT MINUTE(NOW());	#获取当前分钟
mysql> SELECT SECOND(NOW());	#获取当前秒

mysql> SELECT DATE(NOW());		#获取当前日期时间的日期
mysql> SELECT TIME(NOW());		#获取当前日期时间的时间
mysql> SELECT WEEK(NOW());		#获取当前日期为当当前年份第几周内
mysql> SELECT WEEKDAY(NOW());	#获取当前日期为周几(周日是0)
```

- 加密函数

```shell
#加密函数练习

#获取MySQL的root用户密码
mysql> SELECT user,host,authentication_string FROM mysql.user
    -> WHERE
    -> user='root' AND host='localhost';

#PASSWORD()函数加密
mysql> SELECT PASSWORD('123qqq...A');

#MD5()散列加密
mysql> SELECT MD5('123');
```

##### 数学计算

```mysql
#包括+、-、*、/、%操作
#SELECT可以直接运行计算表达式
#也可以对表内已有的数据进行运算

#数学计算练习
mysql> SELECT * FROM tarena.salary WHERE employee_id=8;		#查询8号员工的工资条

mysql> SELECT * FROM tarena.salary 
    -> WHERE
    -> employee_id=8 AND date='20201010';				#查询8号员工2020年10月的工资情况
    
#计算8号员工2020年10月的总工资(总工资=基础工资+奖金)
mysql> SELECT 
    -> date AS 发薪日期,
    -> employee_id AS 工号,
    -> basic AS 基础工资,
    -> bonus AS 奖金,
    -> basic+bonus AS 工资总额
    -> FROM
    -> tarena.salary
    -> WHERE 
    -> employee_id=8 AND date='20201010';		#可以根据已有数据创建临时字段
+--------------+--------+--------------+--------+--------------+
| 发薪日期       | 工号   |  基础工资     | 奖金    | 工资总额      |
+--------------+--------+--------------+--------+--------------+
| 2020-10-10   |      8 |        24247 |   6000 |        30247 |
+--------------+--------+--------------+--------+--------------+

mysql> SELECT * FROM tarena.employees
    -> WHERE
    -> employee_id%2=1;							#查询工号是奇数的员工表记录
```

##### 流程控制函数

- IF函数

```mysql
#语法：
	IF(表达式,值1,值2)
	如果表达式为真，则返回值1，如果表达式为假，则返回值2
	
#IF函数练习
mysql> SELECT IF(1>0,"true","false");		#表达式为真，返回true
+------------------------+
| IF(1>0,"true","false") |
+------------------------+
| true                   |
+------------------------+
1 row in set (0.00 sec)

mysql> SELECT IF(1<0,"true","false");		#表达式为假，返回false
+------------------------+
| IF(1<0,"true","false") |
+------------------------+
| false                  |
+------------------------+
1 row in set (0.00 sec)

#IF函数应用
mysql> SELECT name,uid,IF(uid<1000,"系统用户","普通用户") AS 用户类型
    -> FROM tarena.user;					#根据uid大小来判断用户类型
```

- IFNULL函数

```mysql
#语法：
	IFNULL(值1,值2)
	如果值1不为NULL(空)则返回值1，为NULL(空)则返回值2
	
#IFNULL函数练习
mysql> SELECT IFNULL("hello","world");	#hello不为NULL，返回hello
+-------------------------+
| IFNULL("hello","world") |
+-------------------------+
| hello                   |
+-------------------------+
1 row in set (0.00 sec)

mysql> SELECT IFNULL("","world");		#""不是NULL，是0个字符，返回0个字符
+--------------------+
| IFNULL("","world") |
+--------------------+
|                    |
+--------------------+
1 row in set (0.00 sec)

mysql> SELECT IFNULL(NULL,"world");		#NULL是空，返回world	
+----------------------+
| IFNULL(NULL,"world") |
+----------------------+
| world                |
+----------------------+
1 row in set (0.00 sec)

#IFNULL函数应用
mysql> SELECT 
    -> name AS 用户名,
    -> IFNULL(homedir,"no homedir") AS 用户家目录
    -> FROM tarena.user;				#查询user表中所有记录，如果homedir为空打印no homedir
```

- CASE语句

```mysql
#语法：
	用于多分支判断
	如果字段名等于某个值，则返回对应位置then后面的值并结束判断
	如果与所有值都不相等，则返回else后面的结果并结束判断	

	语法1：
		CASE 字段名              
		WHEN 值1 THEN 结果 
		WHEN 值2 THEN 结果  
		WHEN 值3 THEN 结果 
		ELSE 结果  
		END
	语法2：
		CASE              
		WHEN  判断条件 THEN 结果 
		WHEN  判断条件 THEN 结果  
		WHEN  判断条件 THEN 结果 
		ELSE 结果  
		END



#CASE语句练习

mysql> SELECT * FROM tarena.departments;
+---------+-----------+
| dept_id | dept_name |
+---------+-----------+
|       1 | 人事部    |
|       2 | 财务部    |
|       3 | 运维部    |
|       4 | 开发部    |
|       5 | 测试部    |
|       6 | 市场部    |
|       7 | 销售部    |
|       8 | 法务部    |
+---------+-----------+
8 rows in set (0.00 sec)


#语法1练习
mysql> SELECT dept_id,dept_name, 
	-> CASE dept_name 
	-> WHEN "运维部" THEN "技术部门"
    -> WHEN "开发部" THEN "技术部门" 
    -> WHEN "测试部" THEN "技术部门" 
    -> ELSE "非技术部门" 
    -> END AS "部门类型" 
    -> FROM tarena.departments;
+---------+-----------+-----------------+
| dept_id | dept_name | 部门类型        |
+---------+-----------+-----------------+
|       1 | 人事部    | 非技术部门      |
|       2 | 财务部    | 非技术部门      |
|       3 | 运维部    | 技术部门        |
|       4 | 开发部    | 技术部门        |
|       5 | 测试部    | 技术部门        |
|       6 | 市场部    | 非技术部门      |
|       7 | 销售部    | 非技术部门      |
|       8 | 法务部    | 非技术部门      |
+---------+-----------+-----------------+
8 rows in set (0.00 sec)

#语法2练习
mysql> SELECT dept_id,dept_name,
    -> CASE
    -> WHEN dept_name IN ("运维部","开发部","测试部") THEN "技术部门"
    -> WHEN dept_name IN ("市场部","销售部") THEN "营销部门"
    -> ELSE "职能部门"
    -> END AS "部门类型"
    -> FROM tarena.departments;
+---------+-----------+--------------+
| dept_id | dept_name | 部门类型     |
+---------+-----------+--------------+
|       1 | 人事部    | 职能部门     |
|       2 | 财务部    | 职能部门     |
|       3 | 运维部    | 技术部门     |
|       4 | 开发部    | 技术部门     |
|       5 | 测试部    | 技术部门     |
|       6 | 市场部    | 营销部门     |
|       7 | 销售部    | 营销部门     |
|       8 | 法务部    | 职能部门     |
+---------+-----------+--------------+
8 rows in set (0.00 sec)
```

##### 查询结果处理

即对于SELECT语句从表中查询到的数据进行二次处理

语法：

​	SELECT  字段列表  FROM 库名.表名 WHERE 筛选条件  [分组|排序|过滤|分页];

- 分组(GROUP BY)

```mysql
#分组语法
	SELECT 字段列表 FROM 库名.表名 [WHERE 筛选条件] GROUP BY 分组字段;
	除分组字段外其他字段需配合聚集函数使用
	
#分组练习
mysql> SELECT COUNT(name),shell FROM tarena.user 
	-> GROUP BY shell;								#查询user表中使用各种解释器的用户数量
+-------------+----------------+
| COUNT(name) | shell          |
+-------------+----------------+
|           3 | NULL           |
|           2 | /bin/bash      |
|           1 | /bin/false     |
|           1 | /bin/sync      |
|           1 | /sbin/halt     |
|          20 | /sbin/nologin  |
|           1 | /sbin/shutdown |
+-------------+----------------+
7 rows in set (0.00 sec)

mysql> SELECT dept_id,COUNT(name) FROM tarena.employees 
	-> GROUP BY dept_id;							#查询employees表中不同部门的人数
+---------+-------------+
| dept_id | COUNT(name) |
+---------+-------------+
|       1 |           8 |
|       2 |           5 |
|       3 |           6 |
|       4 |          55 |
|       5 |          12 |
|       6 |           9 |
|       7 |          35 |
|       8 |           3 |
+---------+-------------+
8 rows in set (0.00 sec)
```

- 排序(ORDER BY)

```mysql
#排序语法
	SELECT 字段列表 FROM 库名.表名 [WHERE 筛选条件] ORDER BY 排序字段 [ASC|DESC];
	ASC代表升序，为默认值
	DESC为为降序
	
#排序练习
mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE uid IS NOT NULL;					#默认情况uid无序
    
mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE uid IS NOT NULL
    -> ORDER BY uid;							#按照uid字段值大小升序排列
    
mysql> SELECT name,uid,shell FROM tarena.user
    -> WHERE uid IS NOT NULL
    -> ORDER BY uid DESC;						#按照uid字段值大小降序排列
```

- 过滤(HAVING)

```mysql
#过滤语法
	SELECT 字段列表 FROM 库名.表名 [WHERE 筛选条件] [GROUP BY 分组字段] HAVING 过滤条件;
	#WHERE用于表内真实字段筛选
	#HAVING用于SELECT后出现的字段过滤(可过滤临时字段)
	#GROUP BY 后边只能用HAVING
	
#过滤练习
mysql> SELECT dept_id,name FROM tarena.employees;	#查询employees表所有数据

mysql> SELECT dept_id,name FROM tarena.employees
    -> WHERE dept_id >= 5;							#查询employees表中部门id大于5的记录
    
mysql> SELECT dept_id,COUNT(name) FROM tarena.employees 
	-> WHERE dept_id >= 5 
	-> GROUP BY dept_id;							#查询employees表中部门id大于5的每个部门人数

mysql> SELECT dept_id,COUNT(name) AS dept_count 
	-> FROM tarena.employees 
	-> WHERE dept_id >= 5 
	-> GROUP BY dept_id 
	-> HAVING dept_count > 10;						#查询employees表中部门id大于5且部门人数大于10人的部门与人数
```

- 分页(LIMIT)

```mysql
#分页语法
	SELECT 字段列表 FROM 库名.表名 LIMIT 数字;
	SELECT 字段列表 FROM 库名.表名 LIMIT 数字1,数字2;
	用于显示部分查询结果
	LIMIT 后边只有1个数字则为前几行
	LIMIT 后边有两个数字则从第几行开始及之后的行数(注意：起始行从0开始算)
	
#分页练习
mysql> SELECT * FROM tarena.user;			#显示所有结果

mysql> SELECT * FROM tarena.user LIMIT 2;	#显示所有结果的前2行
		
mysql> SELECT * FROM tarena.user LIMIT 2,3;	#显示所有结果从第3行开始及之后的3行
```

- 综合练习

```mysql
#综合语法：
	SELECT 查询字段列表
	FROM 库名.表名
	WHERE 筛选条件
	GROUP BY 分组字段
	HAVING 过滤字段
	ORDER BY 排序字段
	LIMIT 行数

#1、查询salary表中所有员工2018年工资总和并按照总工资降序排列
mysql> SELECT * FROM tarena.salary;							#获取salary表所有数据

mysql> SELECT * FROM tarena.salary WHERE YEAR(date)=2018;	#筛选2018年工资记录

mysql> SELECT employee_id,basic+bonus AS total 
	-> FROM tarena.salary 
	-> WHERE YEAR(date)=2018;								#通过计算汇总月工资
	
mysql> SELECT employee_id,SUM(basic+bonus) AS year_total 	
	-> FROM tarena.salary 
	-> WHERE YEAR(date)=2018 
	-> GROUP BY employee_id;								#补充分组和SUM函数汇总年工资
	
mysql> SELECT employee_id,SUM(basic+bonus) AS year_total 
	-> FROM tarena.salary 
	-> WHERE YEAR(date)=2018 
	-> GROUP BY employee_id 
	-> HAVING year_total>300000;							#补充过滤年工资高于30w
	
mysql> SELECT employee_id,SUM(basic+bonus) AS year_total 
	-> FROM tarena.salary 
	-> WHERE YEAR(date)=2018 
	-> GROUP BY employee_id 
	-> HAVING year_total>300000 
	-> ORDER BY year_total DESC;							#补充按照年工资降序排列

mysql> SELECT employee_id,SUM(basic+bonus) AS year_total 
	-> FROM tarena.salary 
	-> WHERE YEAR(date)=2018 
	-> GROUP BY employee_id 
	-> HAVING year_total>300000 
	-> ORDER BY year_total DESC
    -> LIMIT 5;												#补充显示前5条记录
    
mysql> SELECT employee_id,SUM(basic+bonus) AS year_total 
	-> FROM tarena.salary 
	-> WHERE YEAR(date)=2018 
	-> GROUP BY employee_id 
	-> HAVING year_total>300000 
	-> ORDER BY year_total DESC,employee_id DESC 
	-> LIMIT 5;												#补充多字段排序
```

#### 连接查询(联表查询)

##### 表关系

![1683789605395](RDBMS01-DAY02.assets/1683789605395.png)

##### 什么是连接查询

把多张表通过连接条件临时组成一张新表，在临时的新表里有连接表的所有字段和数据

##### 连接查询分类

- 按功能分类

  - 内连接
  - 外连接

- 按年代分类

  - SQL92标准：仅支持内连接
  - SQL99标准：支持所有类型连接

- 语法

  ```mysql
  #连接查询语法
  SELECT 字段列表
  FROM 
  表1 AS 别名1
  	连接类型 JOIN
  表2 AS 别名2
  	ON 连接条件
  	连接类型 JOIN
  	...
  表n AS 别名n
  	ON 连接条件
  [WHERE 分组前筛选条件]
  [GROUP BY 分组字段]
  [HAVING 分组后筛选条件]
  [ORDER BY 排序字段]
  [LIMIT 显示行数]
  ```

##### 笛卡尔积

​	笛卡尔乘积是指在数学中，两个集合*X*和*Y*的笛卡尔积（Cartesian product），又称直积，表示为*X*×*Y*，第一个对象是*X*的成员而第二个对象是*Y*的所有可能有序对的其中一个成员

​	例如：X=(1,2), Y=(a,b) 则X×Y=((1,a),(1,b),(2,a),(2,b))

```mysql
#获取笛卡尔积结果
mysql> USE tarena;

mysql> SELECT * FROM departments;				#查询departments表所有数据

mysql> SELECT * FROM employees;					#查询employees表所有数据

mysql> SELECT * FROM departments,employees;		#查询dep表和emp表的笛卡尔积
```

##### 内连接(INNER)

- 功能：将<font color="red">2张及以上</font>的表格按照<font color="red">连接条件</font>连接为1张新表(取<font color="red">符合连接条件</font>的部分)

- 语法

  ```mysql
  #语法格式
  SELECT 字段列表
  FROM 
  表1 AS 别名1
  	INNER JOIN
  表2 AS 别名2 
  	连接条件
  	INNER JOIN
  	...
  表n AS 别名n
  	ON 连接条件
  [WHERE 分组前筛选条件 | GROUP BY 分组字段 | HAVING 分组后筛选条件 | ORDER BY 排序字段 | LIMITE 显示行数]
  
  #连接条件
  	- 等值连接：连接条件是等值判断
  	- 不等值连接：连接条件是不等值判断
  	- 自连接：自己连接自己，把1张表当做2张表(使用时需定义别名)
  ```

- 等值连接

```mysql
#内连接-等值连接练习

#查询每个员工所属部门(多表中无重复字段可直接查询字段)
mysql> SELECT dept_name,name FROM
    -> departments
    -> INNER JOIN
    -> employees
    -> ON departments.dept_id=employees.dept_id;
    
#查询工号为8的员工姓名和所属部门
mysql> SELECT name,dept_name FROM 
	-> employees
    -> INNER JOIN
    -> departments
    -> ON employees.dept_id=departments.dept_id 
    -> WHERE employees.employee_id=8;
    
#查询工号为8的员工姓名和所属部门(对表定义别名后字段前表名可使用别名)
mysql> SELECT 
	-> employees.name,departments.dept_name FROM
    -> employees
    -> INNER JOIN
    -> departments
    -> ON employees.dept_id=departments.dept_id
    -> WHERE employees.employee_id=8;				#原始写法，不定义表的别名
    
mysql> SELECT 
	-> e.name,d.dept_name FROM 
	-> employees AS e
    -> INNER JOIN
    -> departments AS d 
    -> ON e.dept_id=d.dept_id 
    -> WHERE e.employee_id=8;						#优化写法，定义表别名
    
mysql> SELECT 
    -> e.name AS "员工姓名",
    -> d.dept_name AS "部门名称"
    -> FROM
    -> employees AS e
    -> INNER JOIN
    -> departments AS d
    -> ON e.dept_id=d.dept_id
    -> WHERE e.employee_id = 8;						#对查询结果字段名定义别名
+--------------+--------------+
| 员工姓名      | 部门名称       |
+--------------+--------------+
| 汪云          | 人事部        |
+--------------+--------------+
1 row in set (0.00 sec)

##综合练习：查询2018年工资总和超过30w的员工工号、员工姓名及年工资，结果按年工资降序排列，显示前5条即可

#连接employees表和salary表所有数据，能够获取到员工姓名和工资情况
mysql> SELECT employees.employee_id,employees.name,salary.basic,salary.bonus FROM employees INNER JOIN salary ON employees.employee_id=salary.employee_id;

#筛选2018年的工资记录
mysql> SELECT employees.employee_id,employees.name,salary.basic,salary.bonus FROM  employees INNER JOIN salary ON employees.employee_id=salary.employee_id WHERE YEAR(salary.date)=2018;

#计算月度总工资
mysql> SELECT employees.employee_id,employees.name,salary.basic+salary.bonus AS month_total FROM  employees INNER JOIN salary ON employees.employee_id=salary.employee_id WHERE YEAR(salary.date)=2018;

#计算年度总工资
mysql> SELECT employees.employee_id,employees.name,SUM(salary.basic+salary.bonus) AS year_total FROM  employees INNER JOIN salary ON employees.employee_id=salary.employee_id WHERE YEAR(salary.date)=2018 GROUP BY salary.employee_id;

#补充过滤年薪大于30w结果，并降序排列，显示前5条
mysql> SELECT employees.employee_id,employees.name,SUM(salary.basic+salary.bonus) AS year_total FROM  employees INNER JOIN salary ON employees.employee_id=salary.employee_id WHERE YEAR(salary.date)=2018 GROUP BY salary.employee_id HAVING year_total>300000 ORDER BY year_total DESC LIMIT 5;
+-------------+-----------+------------+
| employee_id | name      | year_total |
+-------------+-----------+------------+
|         117 | 和林      |     374923 |
|          31 | 刘海燕    |     374923 |
|          37 | 朱淑兰    |     362981 |
|          68 | 柴冬梅    |     360923 |
|          48 | 范秀英    |     359923 |
+-------------+-----------+------------+
5 rows in set (0.00 sec)

#定义表别名和字段别名，简化SQL语句
mysql> SELECT  e.employee_id AS "工号",  e.name AS "姓名",  SUM(s.basic+s.bonus) AS "年工资"  FROM employees AS e  INNER JOIN  salary AS s  ON e.employee_id=s.employee_id  WHERE YEAR(s.date)=2018 GROUP BY e.employee_id  HAVING 年工资>300000  ORDER BY 年工资 DESC LIMIT 5;
+--------+-----------+-----------+
| 工号   | 姓名      | 年工资    |
+--------+-----------+-----------+
|     31 | 刘海燕    |    374923 |
|    117 | 和林      |    374923 |
|     37 | 朱淑兰    |    362981 |
|     68 | 柴冬梅    |    360923 |
|     48 | 范秀英    |    359923 |
+--------+-----------+-----------+
5 rows in set (0.00 sec)

#折行写法，便于查看
mysql> SELECT 
	-> e.employee_id AS "工号", 
	-> e.name AS "姓名", 
	-> SUM(s.basic+s.bonus) AS "年工资" 
	-> FROM
	-> employees AS e 
	-> INNER JOIN 
	-> salary AS s 
	-> ON e.employee_id=s.employee_id 
	-> WHERE YEAR(s.date)=2018
    -> GROUP BY e.employee_id 
    -> HAVING 年工资>300000 
    -> ORDER BY 年工资 DESC
    -> LIMIT 5;
```

- 非等值连接

```mysql
#内连接-非等值连接

#创建新表，用于划分工资级别
mysql> USE tarena;									#切换到tarena库

mysql> CREATE TABLE tarena.wage_grade(
    -> id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    -> grade CHAR(1),
    -> floor INT,
    -> ceiling INT
    -> );											#创建工资级别表wage_grade

mysql> INSERT INTO tarena.wage_grade(grade,floor,ceiling)
    -> VALUES
    -> ('A',5000,8000),('B',8001,10000),
    -> ('C',10001,15000),('D',15001,20000),
    -> ('E',20001,1000000);							#向表内写入数据，划分工资为5个级别

mysql> SELECT * FROM tarena.wage_grade;				#确认数据写入成功
+----+-------+---------+---------+
| id | grade | floor   | ceiling |
+----+-------+---------+---------+
|  1 | A     |    5000 |    8000 |
|  2 | B     |    8001 |   10000 |
|  3 | C     |   10001 |   15000 |
|  4 | D     |   15001 |   20000 |
|  5 | E     |   20001 | 1000000 |
+----+-------+---------+---------+

#查询2018年12月员工基本工资级别
mysql> SELECT s.employee_id,s.date,s.basic,g.grade FROM salary AS s INNER JOIN wage_grade AS g ON s.basic BETWEEN g.floor AND g.ceiling WHERE YEAR(s.date)=2018 AND MONTH(s.date)=12;
    
#查询2018年12月员工基本工资各级别的人数
mysql> SELECT COUNT(s.basic),g.grade FROM  salary AS s INNER JOIN wage_grade AS g ON s.basic BETWEEN g.floor AND g.ceiling WHERE YEAR(s.date)=2018 AND MONTH(s.date)=12 GROUP BY g.grade;

#查询2018年12月员工基本工资级别和员工姓名(3表联查)
mysql> SELECT  e.name,s.basic,g.grade FROM employees AS e INNER JOIN salary AS s ON e.employee_id=s.employee_id INNER JOIN wage_grade AS g ON s.basic BETWEEN g.floor AND g.ceiling WHERE YEAR(s.date)=2018 AND MONTH(s.date)=12;

mysql> SELECT 
	-> e.name AS 姓名,
    -> s.basic AS 基础工资,
    -> g.grade AS 工资级别
    -> FROM 
    -> employees AS e INNER JOIN salary AS s
    -> ON e.employee_id=s.employee_id 
    -> INNER JOIN wage_grade AS g
    -> ON s.basic BETWEEN g.floor AND g.ceiling
    -> WHERE
    -> YEAR(s.date)=2018 AND MONTH(s.date)=12;			#折行写法，便于查看
```

- 自连接

```mysql
#内连接-自连接
#操作方法：自己连接自己，通过定义别名的方式区分筛选字段

#自连接练习

#查询入职月份与出生月份相同的人有哪些
mysql> SELECT e.employee_id,e.name,e.birth_date,emp.hire_date FROM employees AS e INNER JOIN employees AS emp ON e.employee_id = emp.employee_id WHERE MONTH(e.birth_date)=MONTH(emp.hire_date);

mysql> SELECT employee_id,name,birth_date,hire_date FROM employees WHERE MONTH(birth_date)=MONTH(hire_date);				#可通过WHERE条件实现
```

##### 外连接

| 连接类型 | 关键字                                | 功能                                                         |
| -------- | ------------------------------------- | ------------------------------------------------------------ |
| 左外连接 | <font color="red">LEFT </font>  JOIN  | 左边的表为主表<br>左边表的记录全都显示出来<br>右边的表只显示与条件匹配记录<br>右边表比左边表少的记录使用NULL匹配 |
| 右外连接 | <font color="red">RIGHT</font>  JOIN  | 右边的表为主表<br/>右边表的记录全都显示出来<br/>左边的表只显示与条件匹配记录
左边表比右边表少的记录使用NULL匹配 |
| 全外连接 | <font color="red">UNION</font>  [ALL] | 也称联合查询<br/>用来一起输出多个select查询结果
要求查询时多个select语句查看的字段个数必须一致
UNION关键字默认去重，可以使用UNION ALL包含重复项 |
- 左外连接

```mysql
#语法
	SELECT  字段列表 FROM
    表1 AS 别名1
    LEFT JOIN
    表2 AS 别名2
    ON  连接条件
	[WHERE 筛选条件] | [GROUP BY 分组] | [HAVING 分组后筛选]|[ORDER BY 排序]|[LIMIT 行数]

#左外连接练习

#departments表中创建新部门
mysql> INSERT INTO tarena.departments(dept_name) VALUES ('行政部'),('公关部');

mysql> SELECT * FROM tarena.departments;			#确认新部门添加成功

#测试左外连接
mysql> SELECT d.dept_id,d.dept_name,e.name FROM departments AS d LEFT JOIN employees AS e ON d.dept_id=e.dept_id;
+---------+-----------+-----------+
| dept_id | dept_name | name      |
+---------+-----------+-----------+
|       1 | 人事部    | 梁伟      |
...
|       8 | 法务部    | 杨金凤    |
|       9 | 行政部    | NULL      |		#name为employees表字段，目前部门内没人，用NULL补
|      10 | 公关部    | NULL      |
+---------+-----------+-----------+
135 rows in set (0.00 sec)

#查询目前还没有人的部门
mysql> SELECT 
	-> d.dept_name AS 部门名称,COUNT(e.name) AS 部门人数
    -> FROM departments AS d LEFT JOIN employees AS e
    -> ON d.dept_id=e.dept_id 
    -> GROUP BY d.dept_name 
    -> HAVING 部门人数=0;
+--------------+--------------+
| 部门名称     | 部门人数     |
+--------------+--------------+
| 公关部       |            0 |
| 行政部       |            0 |
+--------------+--------------+
2 rows in set (0.00 sec)
```

- 右外连接

```mysql
#语法
	SELECT  字段列表 FROM
    表1 AS 别名1
    RIGHT JOIN
    表2 AS 别名2
    ON  连接条件
	[WHERE 筛选条件] | [GROUP BY 分组] | [HAVING 分组后筛选]|[ORDER BY 排序]|[LIMIT 行数]
	
#右外连接练习

#employees表中入职新员工
mysql> INSERT INTO employees(name) VALUES ('tom'),('bob');
mysql> SELECT * FROM employees;						#确认新员工添加成功

#测试右外连接
mysql> SELECT d.dept_name,e.name FROM departments AS d RIGHT JOIN employees AS e ON d.dept_id=e.dept_id;
+-----------+-----------+
| dept_name | name      |
+-----------+-----------+
| 人事部    | 梁伟      |
...
| 法务部    | 杨金凤    |
| NULL      | tom       |	#dept_name为departments表字段，目前tom和jim无部门归属，用NULL补
| NULL      | bob       |
+-----------+-----------+
135 rows in set (0.00 sec)
```

- 全外连接

```mysql
#语法：
	(SELECT语句 ) UNION (SELECT语句);			#去除重复结果
	(SELECT语句 ) UNION  ALL (SELECT语句);		#保留重复结果

#全外连接练习

#测试全外连接
mysql> SELECT name,uid,shell FROM user LIMIT 1;		#1条结果

mysql> SELECT name,uid,shell FROM user LIMIT 2;		#2条结果

mysql> (SELECT name,uid,shell FROM user LIMIT 1) 
	-> UNION 
    -> (SELECT name,uid,shell FROM user LIMIT 2);	#去重显示
+------+------+---------------+
| name | uid  | shell         |
+------+------+---------------+
| root |    0 | /bin/bash     |
| bin  |    1 | /sbin/nologin |
+------+------+---------------+
2 rows in set (0.00 sec)

mysql> (SELECT name,uid,shell FROM user LIMIT 1) 
	-> UNION ALL 
	-> (SELECT name,uid,shell FROM user LIMIT 2);	#不去重显示
+------+------+---------------+
| name | uid  | shell         |
+------+------+---------------+
| root |    0 | /bin/bash     |
| root |    0 | /bin/bash     |
| bin  |    1 | /sbin/nologin |
+------+------+---------------+
3 rows in set (0.00 sec)

#左外连接 UNION 右外连接实现全外连接
mysql> (SELECT d.dept_name,e.name FROM departments d LEFT JOIN employees e ON d.dept_id=e.dept_id) 
	-> UNION
    -> (SELECT d.dept_name,e.name FROM departments d RIGHT JOIN employees e ON d.dept_id=e.dept_id);
+-----------+-----------+
| dept_name | name      |
+-----------+-----------+
| 人事部    | 梁伟      |
...
| 法务部    | 杨金凤    |
| 行政部    | NULL      |
| 公关部    | NULL      |
| NULL      | tom       |
| NULL      | bob       |
+-----------+-----------+
137 rows in set (0.00 sec)

#综合练习：查询2018年、2019年、2020年， 01月10号总工资最高的员工编号和总工资
mysql> (SELECT e.name,s.basic+s.bonus AS total FROM employees AS e INNER JOIN salary AS s ON e.employee_id=s.employee_id  WHERE date='20180110' ORDER BY total DESC LIMIT 1) UNION (SELECT e.name,s.basic+s.bonus AS total FROM employees AS e INNER JOIN salary AS s ON e.employee_id=s.employee_id  WHERE date='20190110' ORDER BY total DESC LIMIT 1) UNION (SELECT e.name,s.basic+s.bonus AS total FROM employees AS e INNER JOIN salary AS s ON e.employee_id=s.employee_id  WHERE date='20200110' ORDER BY total DESC LIMIT 1);
+-----------+-------+
| name      | total |
+-----------+-------+
| 朱淑兰    | 34152 |
| 朱淑兰    | 35309 |
| 邢成      | 37800 |
+-----------+-------+
3 rows in set (0.00 sec)
```

